<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


require 'vendor/autoload.php';

$app = new \Slim\App(['settings' => [
        'displayErrorDetails' => true]]);

$app->add(new \Slim\Middleware\JwtAuthentication([
    "path" => ["/formularios", "/dados"], /* or ["/api", "/admin"] */
    "secure" => true,
    "relaxed" => "localhost",
    "secret" => "endicontolken"
]));

$app->post('/formularios/desvios/novo_desvio', function (Request $req, Response $res, $args) {
    echo 'hello world';
});

$app->get('/', function (Request $req, Response $res, $args) {
    return $res->withStatus(303)->withHeader('Location', '/index.html');
});

$app->get('/formularios/desvios', function (Request $req, Response $res, $args) {
    echo 'hello world';
});
$app->get('/formularios/desvios/{id}', function (Request $req, Response $res, $args) {
    echo 'hello';
});
$app->delete('/formularios/desvios/{id}', function (Request $req, Response $res, $args) {
    echo 'hello world';
});
$app->put('/formularios/desvios/{id}', function (Request $req, Response $res, $args) {
    echo 'hello world';
});
$app->post('/formularios/desvios/novo_desvio', function (Request $req, Response $res, $args) {
    echo 'hello world';
});
$app->get('/formularios/relatos', function (Request $req, Response $res, $args) {
    echo 'hello world';
});
$app->get('/formularios/relatos/{id}', function (Request $req, Response $res, $args) {
    echo 'hello world';
});
$app->delete('/formularios/relatos/{id}', function (Request $req, Response $res, $args) {
    echo 'hello world';
});
$app->put('/formularios/relatos/{id}', function (Request $req, Response $res, $args) {
    echo 'hello world';
});
$app->post('/formularios/relatos/novo_relato', function (Request $req, Response $res, $args) {
    echo 'hello world';
});
$app->get('/formularios/relatorios_de_investigacao/', function (Request $req, Response $res, $args) {
    echo 'hello world';
});
$app->get('/formularios/relatorios_de_investigacao/{id}', function (Request $req, Response $res, $args) {
    echo 'hello world';
});
$app->delete('/formularios/relatorios_de_investigacao/{id}', function (Request $req, Response $res, $args) {
    echo 'hello world';
});
$app->put('/formularios/relatorios_de_investigacao/{id}', function (Request $req, Response $res, $args) {
    echo 'hello world';
});
$app->post('/formularios/relatorios_de_investigacao/novo_relatorio', function (Request $req, Response $res, $args) {
    echo 'hello world';
});
$app->get('/formularios/comunicados', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerComunicado;
    return $res->withJson($control->getComunicados(),200);
});
$app->get('/formularios/comunicados/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerComunicado;
    return $res->withJson($control->getComunicado($args['id']),200);
});
$app->delete('/formularios/comunicados/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerComunicado;
    $control->deleteComunicado($args['id']);
    return $res->withStatus(204);
});
$app->put('/formularios/comunicados/{id}', function (Request $req, Response $res, $args) {
    $control = new App\Controllers\ControllerComunicado();
    $control->putComunicado($args['id'], $req->getParsedBody());
    return $res->withStatus(201);
});
$app->post('/formularios/comunicados/novo_comunicado', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerComunicado();
    $data= $req->getParsedBody();
    $req->withParsedBody(null);
    return $req->withJson($control->postComunicado($data),201);
});
$app->get('/formularios/analisesrisco', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerAnaliseRisco;
    return $res->withJson($control->getAnalisesRisco(),200);
});
$app->get('/formularios/analisesrisco/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerAnaliseRisco;
    return $res->withJson($control->getAnaliseRisco($args()["id"]),200);
});
$app->delete('/formularios/analisesrisco/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerAnaliseRisco;
    $control->deleteAnaliseRisco($args['id']);
    return $res->withStatus(204);
});
$app->put('/formularios/analisesrisco/{id}', function (Request $req, Response $res, $args) {
    $control = new App\Controllers\ControllerAnaliseRisco;
    $control->putAnaliseRisco($args['id'], $req->getParsedBody());
    return $res->withStatus(201);
});
$app->post('/formularios/analisesrisco/nova_analiserisco', function (Request $req, Response $res, $args) {
    $control = new App\Controllers\ControllerAnaliseRisco;
    $data=$req->getParsedBody();
    $req->withParsedBody(null);
    return $res->withJson($control->postAnaliseRisco($data),201);
});
$app->get('/dados/funcoes', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerFuncao;
    return $res->withJson($control->getFuncoes(),200);
});
$app->get('/dados/funcoes/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerFuncao;
    return $res->withJson($control->getFuncao($args['id']),200);
});
$app->delete('/dados/funcoes/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerFuncao;
    $control->deleteFuncao($args['id']);
    return $res->withStatus(204);
});
$app->put('/dados/funcoes/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerFuncao;
    $control->putFuncao($args['id'], $req->getParsedBody());
    return $res->withStatus(201);
});
$app->post('/dados/funcoes/nova_funcao', function (Request $req, Response $res, $args) {
    $control = new App\Controllers\ControllerFuncao;
    $data= $req->getParsedBody();
    $req->withParsedBody(null);
    return $res->withJson($control->postFuncao($data),201);
});
$app->get('/dados/funcionarios', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerFuncionario;
    return $res->withJson($control->getFuncionarios(),200);
});
$app->get('/dados/funcionarios/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerFuncionario;
    return $res->withJson($control->getFuncionario($args['id']),200);
});
$app->delete('/dados/funcionarios/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerFuncionario;
    $control->deleteFuncionario($args['id']);
    return $res->withStatus(204);
});
$app->put('/dados/funcionarios/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerFuncionario;
    $control->putFuncionario($args['id'], $req->getParsedBody());
    return $res->withStatus(201);
});
$app->post('/dados/funcionarios/novo_funcionario', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerFuncionario;
    $data = $req->getParsedBody();
    $req->withParsedBody(null);
    return $res->withJson($control->postFuncionario($data),201);
});
$app->get('/dados/locais', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerLocal;
    return $res->withJson($control->getLocais(),200);
});
$app->get('/dados/locais/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerLocal;
    return $res->withJson($control->getLocal($args['id']),200);
});
$app->delete('/dados/locais/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerLocal;
    $control->deleteLocal($args['id']);
    return $res->withStatus(204);
});
$app->put('/dados/locais/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerLocal;
    $control->putLocal($args['id'], $req->getParsedBody());
    return $res->withStatus(201);
});
$app->post('/dados/locais/novo_local', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerLocal;
    $data= $req->getParsedBody();
    $req->withParsedBody(null);
    return $res->withJson($control->postLocal($data), 201);
});
$app->get('/dados/localidadespolo', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerLocalidadePolo;
    return $res->withJson($control->getLocalidadesPolo(),200);
});
$app->get('/dados/localidadespolo/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerLocalidadePolo;
    return $res->withJson($control->getLocalidadePolo($args['id']),200);
});
$app->delete('/dados/localidadespolo/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerLocalidadePolo;
    $control->deleteLocalidadePolo($args['id']);
    return $res->withStatus(204);
});
$app->put('/dados/localidadespolo/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerLocalidadePolo;
    $control->putLocalidadePolo($args['id'], $req->getParsedBody());
    return $res->withStatus(201);
});
$app->post('/dados/localidadespolo/nova_localidadepolo', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerLocalidadePolo;
    $data = $req->getParsedBody();
    $req->withParsedBody(null);
    return $res->withJson($control->postLocalidadePolo($data),201);
});
$app->get('/dados/polos', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerPolo;
    return $res->withJson($control->getPolos(),200);
});
$app->get('/dados/polos/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerPolo;
    return $res->withJson($control->getPolo($args()['id']),200);
});
$app->delete('/dados/polos/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerPolo;
    $control->deletePolo($args['id']);
    return $res->withStatus(204);
});
$app->put('/dados/polos/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerPolo;
    $control->putPolo($args['id'], $req->getParsedBody());
    return $res->withStatus(201);
});
$app->post('/dados/polos/novo_polo', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerPolo;
    $data= $req->getParsedBody();
    $req->withParsedBody(null);
    return $res->withJson($control->postPolo($data),201);
});
$app->get('/dados/processos', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerProcesso;
    return $res->withJson($control->getProcessos(),200);
});
$app->get('/dados/processos/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerProcesso;
    return $res->withJson($control->getProcesso($args['id']),200);
});
$app->delete('/dados/processos/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerProcesso;
    $control->deleteProcesso($args['id']);
    return $res->withStatus(204);
});
$app->put('/dados/processos/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerProcesso;
    $control->putProcesso($args['id'], $req->getParsedBody());
    return $res->withStatus(201);
});
$app->post('/dados/processos/novo_processo', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerProcesso;
    $data= $req->getParsedBody();
    $req->withParsedBody(null);
    return $res->withJson($control->postProcesso($data),201);
});
$app->get('/dados/regionais', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerRegional;
    return $res->withJson($control->getRegionais(),200);
});
$app->get('/dados/regionais/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerRegional;
    return $res->withJson($control->getRegional($args['id']),200);
});
$app->delete('/dados/regionais/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerRegional;
    $control->deleteRegional($args['id']);
    return $res->withStatus(204);
});
$app->put('/dados/regionais/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerRegional;
     $control->putRegional($args['id'],$req->getParsedBody());
     return $res->withStatus(201);
});
$app->post('/dados/regionais/nova_regional', function (Request $req, Response $res, $args) {
     $control= new App\Controllers\ControllerRegional;
     $data=$req->getParsedBody();
     $req->withParsedBody(null);
     return $res->withJson($control->postRegional($data),201);
});
$app->get('/dados/residuos', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerResiduo;
    return $res->withJson($control->getResiduos(),200);
});
$app->get('/dados/residuos/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerResiduo;
    return $res->withJson($control->getResiduo($args['id']),200);
});
$app->delete('/dados/residuos/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerResiduo;
    $control->deleteResiduo($args['id']);
    return $res->withStatus(204);
});
$app->put('/dados/residuos/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerResiduo;
    $control->putResiduo($args['id'], $req->getParsedBody());
    return $res->withStatus(201);
});
$app->post('/dados/residuos/novo_residuo', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerResiduo;
    $data= $req->getParsedBody();
    $req->withParsedBody(null);
    return $res->withJson($control->postResiduo($data),201);
});
$app->get('/dados/tiposprocesso', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerTipoProcesso;
    return $res->withJson($control->getTiposProcesso(),200);
});
$app->get('/dados/tiposprocesso/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerTipoProcesso;
    return $res->withJson($control->getTipoProcesso($args['id']),200);
});
$app->delete('/dados/tiposprocesso/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerTipoProcesso;
    $control->deleteTipoProcesso($args['id']);
    $res->withStatus(204);
});
$app->put('/dados/tiposprocesso/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerTipoProcesso;
    $control->putTipoProcesso($args['id'], $req->getParsedBody());
    return $res->withStatus(201);
});
$app->post('/dados/tiposprocesso/novo_tipoprocesso', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerTipoProcesso;
    $data= $req->getParsedBody();
    $req->withParsedBody(null);
    return $res->withJson($control->postTipoProcesso($data),201);
});
$app->get('/dados/equipamentoscoletivos', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerEquipamentoColetivo;
    return $res->withJson($control->getEquipamentos(),200);
});
$app->get('/dados/equipamentoscoletivos/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerEquipamentoColetivo;
    return $res->withJson($control->getEquipamento($args['id']),200);
});
$app->delete('/dados/equipamentoscoletivos/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerEquipamentoColetivo;
    $control->deleteEquipamento($args['id']);
    return $res->withStatus(204);
});
$app->put('/dados/equipamentoscoletivos/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerEquipamentoColetivo;
    $control->putProcesso($args['id'], $req->getParsedBody());
    return $res->withStatus(201);
});
$app->post('/dados/equipamentoscoletivos/novo_equipamentocoletivo', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerEquipamentoColetivo;
    $data = $req->getParsedBody();
    $req->withParsedBody(null);
    return $res->withJson($control->postEquipamento($data),201);
});
$app->get('/dados/equipamentosindividuais', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerEquipamentoIndividual;
    return $res->withJson($control->getEquipamentos(),200);
});
$app->get('/dados/equipamentosindividuais/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerEquipamentoIndividual();
    return $res->withJson($control->getEquipamento($args['id']),200);
});
$app->delete('/dados/equipamentosindividuais/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerEquipamentoIndividual();
    $control->deleteEquipamento($args['id']);
    return $res->withStatus(204);
});
$app->put('/dados/equipamentosindividuais/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerEquipamentoIndividual;
    $control->putProcesso($args['id'], $req->getParsedBody());
    return $res->withStatus(201);
});
$app->post('/dados/equipamentosindividuais/novo_equipamentoindividual', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerEquipamentoIndividual;
    $data= $req->getParsedBody();
    $req->withParsedBody(null);
    return $res->withJson($control->postEquipamento($data),201);
});
$app->get('/dados/perguntasrisco', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerPerguntaRisco;
    return $res->withJson($control->getPerguntas(),200);
});
$app->get('/dados/perguntasrisco/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerPerguntaRisco;
    return $res->withJson($control->getPergunta($args['id']),200);
});
$app->delete('/dados/perguntasrisco/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerPerguntaRisco;
    $control->deletePergunta($args['id']);
    return $res->withStatus(204);
});
$app->put('/dados/perguntasrisco/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerPerguntaRisco;
    $control->putPergunta($args['id'], $req->getParsedBody());
    return $res->withStatus(201);
});
$app->post('/dados/perguntasrisco/nova_perguntarisco', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerPerguntaRisco;
    $data= $req->getParsedBody();
    $req->withParsedBody(null);
    return $res->withJson($control->postPergunta($data),201);
});
$app->get('/dados/respostasrisco', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerRespostaRisco;
    return $res->withJson($control->getRespostas(),200);
});
$app->get('/dados/respostasrisco/{idReferencia}/{idResposta}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerRespostaRisco;
    return $res->withJson($control->getResposta($args['idReferencia'],$args['idResposta']),200);
});
$app->delete('/dados/respostasrisco/{idReferencia}/{idResposta}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerRespostaRisco;
    $control->deleteResposta($args['idReferencia'],$args['idResposta']);
    return $res->withStatus(204);
});
$app->put('/dados/respostasrisco/{idReferencia}/{idResposta}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerRespostaRisco;
    $control->putResposta($args['idReferencia'], $args['idResposta'], $req->getParsedBody());
    return $res->withStatus(201);
});
$app->post('/dados/respostasrisco/nova_resposta', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerRespostaRisco;
    $data= $req->getParsedBody();
    $req->withParsedBody(null);
    return $res->withJson($control->postResposta($data),201);
});
$app->get('/dados/riscosidentificados', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerRiscoIdentificado();
    return $res->withJson($control->getRiscosIdentificados(),200);
});
$app->get('/dados/riscosidentificados/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerRiscoIdentificado();
    return $res->withJson($control->getRiscoIdentificado($args['id']),200);
});
$app->delete('/dados/riscosidentificados/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerRiscoIdentificado();
    $control->deleteRiscoIdentificado($args['id']);
    return $res->withStatus(204);
});
$app->put('/dados/riscosidentificados/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerRiscoIdentificado();
    $control->putRiscoIdentificado($args['id'], $req->getParsedBody());
    return $res->withStatus(201);
});
$app->post('/dados/riscosidentificados/novo_riscoidentificado', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerRiscoIdentificado();
    $data= $req->getParsedBody();
    $req->withParsedBody(null);
    return $res->withJson($control->postRiscoIdentificado($data),201);
});
$app->get('/dados/treinamentos', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerTreinamento();
    return $res->withJson($control->getTreinamentos(),200);
});
$app->get('/dados/treinamentos/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerTreinamento();
    return $res->withJson($control->getTreinamento($args['id']),200);
});
$app->delete('/dados/treinamentos/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerTreinamento();
    $control->deleteTreinamento($args['id']);
    return $res->withStatus(204);
});
$app->put('/dados/treinamentos/{id}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerTreinamento();
    $control->putTreinamento($args['id'], $req->getParsedBody());
    return $res->withStatus(201);
});
$app->post('/dados/treinamentos/novo_treinamento', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerTreinamento();
    $data = $req->getParsedBody();
    $req->withParsedBody(null);
    return $res->withJson($control->postTreinamento($data),201);
});
$app->get('/dados/treinamentosrealizados', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerTreinamentoRealizado();
    return $res->withJson($control->getTreinamentosRealizados(),200);
});
$app->get('/dados/treinamentosrealizados/{idFuncionario}/{idTreinamento}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerTreinamentoRealizado();
    return $res->withJson($control->getTreinamentoRealizado($args['idFuncionario'],$args()['idTreinamento']),200);
});
$app->delete('/dados/treinamentosrealizados/{idFuncionario}/{idTreinamento}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerTreinamentoRealizado();
    $control->deleteTreinamentoRealizado($args['idFuncionario'],$args['idTreinamento']);
    return $res->withStatus(204);
});
$app->put('/dados/treinamentosrealizados/{idFuncionario}/{idTreinamento}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerTreinamentoRealizado();
    $control->putTreinamentoRealizado($args['idFuncionario'], $args['idTreinamento'], $req->getParsedBody());
    return $res->withStatus(201);
});
$app->post('/dados/treinamentosrealizados/{idFuncionario}/{idTreinamento}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerTreinamentoRealizado();
    $control->postTreinamentoRealizado($args['idFuncionario'], $args['idTreinamento'],$req->getParsedBody());
    return $res->withStatus(201);
});
$app->delete('/dados/envolvidoscomunicado/{idReferencia}/{idFuncionario}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerEnvolvidoComunicado();
    $control->deleteEnvolvidoComunicado($args['idReferencia'],$args['idFuncionario']);
    return $res->withStatus(204);
});
$app->post('/dados/envolvidoscomunicado/{idReferencia}/{idFuncionario}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerEnvolvidoComunicado();
    $control->postEnvolvidoComunicado($args['idReferencia'], $args['idFuncionario'],$req->getParsedBody());
    return $res->withStatus(201);
});
$app->delete('/dados/equipamentoscoletivosanaliserisco/{idReferencia}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerEquipamentoColetivoAnaliseRisco();
    $control->deleteEquipamentosColetivosAnaliseRisco($args['idReferencia']);
    return $res->withStatus(204);
});
$app->post('/dados/equipamentoscoletivosanaliserisco/{idReferencia}/{idEquipamentoColetivo}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerEquipamentoColetivoAnaliseRisco();
    $control->postEquipamentoColetivoAnaliseRisco($args['idReferencia'], $args['idEquipamentoColetivo']);
    return $res->withStatus(201);
});
$app->delete('/dados/equipamentosindividuaisanaliserisco/{idReferencia}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerEquipamentoIndividualAnaliseRisco();
    $control->deleteEquipamentosIndividuaisAnaliseRisco($args['idReferencia']);
    return $res->withStatus(204);
});
$app->post('/dados/equipamentosindividuaisanaliserisco/{idReferencia}/{idEquipamentoIndividual}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerEquipamentoIndividualAnaliseRisco();
    $control->postEquipamentoIndividualAnaliseRisco($args['idReferencia'], $args['idEquipamentoIndividual']);
    return $res->withStatus(201);
});
$app->delete('/dados/membrosanaliserisco/{idReferencia}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerMembroAnaliseRisco();
    $control->deleteMembrosAnaliseRisco($args['idReferencia']);
    return $res->withStatus(204);
});
$app->post('/dados/membrosanaliserisco/{idReferencia}/{idMembro}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerMembroAnaliseRisco();
    $control->postMembroAnaliseRisco($args['idReferencia'], $args['idMembro']);
    return $res->withStatus(201);
});
$app->delete('/dados/riscosidentificadosanaliserisco/{idReferencia}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerRiscoIdentificadoAnaliseRisco();
    $control->deleteRiscosIdentificadosAnaliseRisco($args['idReferencia']);
    return $res->withStatus(204);
});
$app->post('/dados/riscosidentificadosanaliserisco/{idReferencia}/{idRiscoIdentificado}', function (Request $req, Response $res, $args) {
    $control= new App\Controllers\ControllerRiscoIdentificadoAnaliseRisco();
    $control->postRiscoIdentificadoAnaliseRisco($args['idReferencia'], $args['idRiscoIdentificado']);
    return $res->withStatus(201);
});
$app->run();
