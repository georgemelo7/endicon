<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Resposta
 *
 * @author ADM
 */
class AnaliseCausa {
    private $id;
    private $relatorioInvestigacao;
    private $resposta;


    public function __construct($id, $resposta, $relatorioInvestigacao) {
        $this->id=$id;
        $this->relatorioInvestigacao=$relatorioInvestigacao;
        $this->resposta=$resposta;

    }
    public function getId() {
        return $this->id;
    }

    public function setId($id){
        $this->id=$id;
    }
    public function getIdPergunta() {
        return $this->idPergunta;
    }

    public function setRelatorioInvestigacao($relatorioInvestigacao){
        $this->relatorioInvestigacao=$relatorioInvestigacao;
    }
    public function getResposta() {
        return $this->resposta;
    }

    public function setResposta($resposta){
        $this->resposta=$resposta;
    }

    public function toArray(){
        $json=array(
            'id'=>  $this->id,
            'relatorioInvestigacao'=>  $this->relatorioInvestigacao,
            'resposta'=>  $this->resposta
        );
        return $json;
    }
}
