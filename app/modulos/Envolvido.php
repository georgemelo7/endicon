<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Envolvido
 *
 * @author ADM
 */
class Envolvido {
    
    private $idFuncionario;
    private $idReferencia;
    
    public function __construct($idFuncionario, $idReferencia) {
        $this->idFuncionario=$idFuncionario;
        $this->idReferencia=$idReferencia;
    }
    
    public function getIdFuncionario() {
        return $this->idFuncionario; 
    }
    
    public function setIdFuncionario($idFuncionario){
        $this->idFuncionario=$idFuncionario;
    }
    public function getIdReferencia() {
        return $this->idReferencia; 
    }
    
    public function setIdReferencia($idReferencia){
        $this->idReferencia=$idReferencia;
    }
    
    public function toArray(){
        $json=array(
            'idFuncionario'=>  $this->idFuncionario,
            'idReferencia'=>  $this->idReferencia
        );
        return $json;
    }
    
}
