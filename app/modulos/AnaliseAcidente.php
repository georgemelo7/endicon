<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of AnaliseAcidente
 *
 * @author George Tassiano
 */
class AnaliseAcidente {
    private $id;
    private $codigo;
    private $descricao;
    
    
    public function __construct($id, $codigo, $descricao) {
        $this->id=$id;
        $this->codigo=$codigo;
        $this->descricao=$descricao;
        
    }
    public function getid() {
        return $this->id; 
    }
    
    public function setId($id){
        $this->id=$id;
    }
    public function getCodigo() {
        return $this->codigo; 
    }
    
    public function setCodigo($codigo){
        $this->codigo=$codigo;
    }
    public function getDescricao() {
        return $this->descricao; 
    }
    
    public function setDescricao($descricao){
        $this->descricao=$descricao;
    }
    
    public function toArray(){
        $json=array(
            'id'=>  $this->id,
            'codigo'=>  $this->codigo,
            'descricao'=>  $this->descricao
        );
        return $json;
    }
}
