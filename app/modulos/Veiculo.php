<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Veiculo
 *
 * @author George Tassiano
 */
class Veiculo {
    private $id;
    private $placa;
    private $tipo;
    private $fabricante;
    private $modelo;
    private $chassi;
    private $renavam;
    private $ano;
    private $idade;
    private $familia;
    
    public function __construct($id, $placa, $tipo, $fabricante, $modelo, $chassi, $renavam, $ano, $idade, $familia) {
        $this->id=$id;
        $this->placa=$placa;
        $this->tipo=$tipo;
        $this->fabricante=$fabricante;
        $this->modelo=$modelo;
        $this->chassi=$chassi;
        $this->renavam=$renavam;
        $this->ano=$ano;
        $this->idade=$idade;
        $this->familia=$familia;
        
    }
    public function getid() {
        return $this->id; 
    }
    
    public function setId($id){
        $this->id=$id;
    }
    public function getPlaca() {
        return $this->placa; 
    }
    
    public function setPlaca($placa){
        $this->placa=$placa;
    }
    public function getTipo() {
        return $this->tipo; 
    }
    
    public function setTipo($tipo){
        $this->tipo=$tipo;
    }
    
    public function getFabricante(){
        return $this->fabricante;
    }
    
    public function setFabricante($fabricante){
        $this->fabricante=$fabricante;
    }
    
    public function getModelo(){
        return $this->modelo;
    }
    
    public function setModelo($modelo){
        $this->modelo=$modelo;
    }
    
    public function getChassi(){
        return $this->chassi;
    }
    
    public function setChassi($chassi){
        $this->chassi=$chassi;
    }
    
    public function getRenavam(){
        return $this->renavam;
    }
    
    public function setRenavam($renavam){
        $this->renavam=$renavam;
    }
    
    public function getAno(){
        return $ano;
    }
    
    public function setAno($ano){
        $this->ano=$ano;
    }
    
    public function getIdade(){
        $this->idade;
    }
    
    public function setIdade($idade){
        $this->idade=$idade;
    }
    
    public function getFamilia(){
        return $this->familia;
    }
    
    public function setFamilia($familia){
        $this->familia=$familia;
    }
    
    public function toArray(){
        $json=array(
            'id'=>  $this->id,
            'placa'=>  $this->placa,
            'tipo'=>  $this->tipo,
            'fabricante'=>  $this->fabricante,
            'modelo'=>  $this->chassi,
            'renavam'=>  $this->renavam,
            'ano'=>  $this->ano,
            'idade'=>  $this->idade,
            'familia'=>  $this->familia
        );
        return $json;
    }
}
