<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Processo
 *
 * @author George Tassiano
 */
class Eficacia {
    private $idFuncionario;
    private $id;
    private $data;


    public function __construct($id, $idFuncionario, $data) {
        $this->id=$id;
        $this->idFuncionario=$idFuncionario;
        $this->data=$data;

    }
    public function getId() {
        return $this->id;
    }

    public function setId($id){
        $this->id=$id;
    }
    public function getIdFuncionario() {
        return $this->idFuncionario;
    }

    public function setIdFuncionario($idFuncionario){
        $this->idFuncionario=$idFuncionario;
    }

    public function getData() {
        return $this->data;
    }

    public function setData($data){
        $this->data=$data;
    }

    public function toArray(){
        $json=array(
            'id'=>  $this->id,
            'idFuncionario'=> $this->idFuncionario,
            'data'=>  $this->data
        );
        return $json;
    }
}
