<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Polo
 *
 * @author George Tassiano
 */
class Polo {
    private $id;
    private $nome;
    private $regional;
    
    public function __construct($id, $nome, $regional) {
        $this->id=$id;
        $this->nome=$nome;
        $this->regional=$regional;
    }
    public function getid() {
        return $this->id; 
    }
    
    public function setId($id){
        $this->id=$id;
    }
    public function getNome() {
        return $this->nome; 
    }
    
    public function setNome($nome){
        $this->nome=$nome;
    }
    public function getRegional() {
        return $this->regional; 
    }
    
    public function setRegional($regional){
        $this->regional=$regional;
    }
    
    public function toArray(){
        $json=array(
            'id'=>  $this->id,
            'nome'=>  $this->nome,
            'regional'=>  $this->regional
        );
        return $json;
    }
}
