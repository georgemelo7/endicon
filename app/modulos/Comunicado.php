<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Modulos;
class Comunicado{
    private $id;
    private $tipo;
    private $dataCriacao;
    private $horaCriacao;
    private $tema;
    private $descricao;
    private $dataOcorrido;
    private $horaOcorrido;
    private $danosMateriais;
    private $danosAmbientais;
    private $local;
    private $funcionario;
    private $residuos;
    private $envolvidos;
    private $tipoProcesso;
    private $analiseRisco;
    
    public function __construct($id, $tipo, $dataCriacao, $horaCriacao, $tema, $descricao,
            $dataOcorrido, $horaOcorrido, $danosMateriais, $danosAmbientais, $local, $funcionario, $tipoProcesso, $residuos=array(), $envolvidos=array(), $analiseRisco=null) {
        $this->id=$id;
        $this->tipo=$tipo;
        $this->dataCriacao=$dataCriacao;
        $this->horaCriacao=$horaCriacao;
        $this->tema=$tema;
        $this->descricao=$descricao;
        $this->dataOcorrido=$dataOcorrido;
        $this->horaOcorrido=$horaOcorrido;
        $this->danosMateriais=$danosMateriais;
        $this->danosAmbientais=$danosAmbientais;
        $this->local=$local;
        $this->funcionario=$funcionario;
        $this->tipoProcesso=$tipoProcesso;
        $this->residuos=$residuos;
        $this->envolvidos=$envolvidos;
        $this->analiseRisco=$analiseRisco;
    }
    
    public function getId() {
        return $this->id; 
    }
    
    public function setId($id){
        $this->id=$id;
    }
    public function getTipo() {
        return $this->tipo; 
    }
    
    public function setTipo($tipo){
        $this->tipo=$tipo;
    }
    public function getDataCriacao() {
        return $this->dataCriacao; 
    }
    
    public function setDataCriacao($dataCriacao){
        $this->dataCriacao=$dataCriacao;
    }
    public function getHoraCriacao() {
        return $this->horaCriacao; 
    }
    
    public function setHoraCriacao($horaCriacao){
        $this->horaCriacao=$horaCriacao;
    }
    public function getTema() {
        return $this->tema; 
    }
    
    public function setTema($tema){
        $this->tema=$tema;
    }
    public function getDescricao() {
        return $this->descricao; 
    }
    
    public function setDescricao($descricao){
        $this->descricao=$descricao;
    }
    public function getDataOcorrido() {
        return $this->dataOcorrido; 
    }
    
    public function setDataOcorrido($dataOcorrido){
        $this->dataOcorrido=$dataOcorrido;
    }
    public function getHoraOcorrido() {
        return $this->horaOcorrido; 
    }
    
    public function setHoraOcorrido($horaOcorrido){
        $this->horaOcorrido=$horaOcorrido;
    }
    public function getDanosMateriais() {
        return $this->danosMateriais; 
    }
    
    public function setDanosMateriais($danosMateriais){
        $this->danosMateriais=$danosMateriais;
    }
    public function getDanosAmbientais() {
        return $this->danosAmbientais; 
    }
    
    public function setDanosAmbientais($danosAmbientais){
        $this->danosAmbientais=$danosAmbientais;
    }
    public function getLocal() {
        return $this->local; 
    }
    
    public function setLocal($local){
        $this->local=$local;
    }
    public function getFuncionario() {
        return $this->funcionario; 
    }
    
    public function setFuncionario($funcionario){
        $this->funcionario=$funcionario;
    }
    public function getResiduos() {
        return $this->residuos; 
    }
    
    public function setResiduos($residuos){
        $this->residuos=$residuos;
    }
    public function getEnvolvidos() {
        return $this->envolvidos; 
    }
    
    public function setEnvolvidos($envolvidos){
        $this->envolvidos=$envolvidos;
    }
    public function getTipoProcesso() {
        return $this->tipoProcesso; 
    }
    
    public function setTipoProcesso($tipoProcesso){
        $this->tipoProcesso=$tipoProcesso;
    }
    public function getAnaliseRisco() {
        return $this->analiseRisco; 
    }
    
    public function setAnaliseRisco($analiseRisco){
        $this->analiseRisco=$analiseRisco;
    }
    
    public function toArray(){
        $json=array(
            'id'=>  $this->id,
            'tipo'=>  $this->tipo,
            'dataCriacao'=>  $this->dataCriacao,
            'horaCriacao'=> $this->horaCriacao,
            'tema'=>  $this->tema,
            'descricao'=>  $this->descricao,
            'dataOcorrido'=>  $this->dataOcorrido,
            'horaOcorrido'=> $this->horaOcorrido,
            'danosMateriais'=>  $this->danosMateriais,
            'danosAmbientais'=> $this->danosAmbientais,
            'local'=>  $this->local,
            'funcionario'=>  $this->funcionario,
            'residuos'=>  $this->residuos,
            'envolvidos'=>  $this->envolvidos,
            'tipoProcesso'=> $this->tipoProcesso,
            'analiseRisco'=>  $this->analiseRisco
        );
        return $json;
    }
}

