<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Regional
 *
 * @author George Tassiano
 */
class Regional {
    private $id;
    private $nome;
    private $estado;
    
    
    public function __construct($id, $nome, $estado) {
        $this->id=$id;
        $this->nome=$nome;
        $this->estado=$estado;
        
    }
    public function getid() {
        return $this->id; 
    }
    
    public function setId($id){
        $this->id=$id;
    }
    public function getNome() {
        return $this->nome; 
    }
    
    public function setNome($nome){
        $this->nome=$nome;
    }
    public function getEstado() {
        return $this->estado; 
    }
    
    public function setEstado($estado){
        $this->estado=$estado;
    }
    
    public function toArray(){
        $json=array(
            'id'=>  $this->id,
            'nome'=>  $this->nome,
            'estado'=>  $this->estado
        );
        return $json;
    }
}
