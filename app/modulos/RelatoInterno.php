<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of RelatoInterno
 *
 * @author George Tassiano
 */
class RelatoInterno {
    private $id;
    private $dataCriacao;
    private $horaCriacao;
    private $horarioInicioTrabalho;
    private $dataAtendimentoMedico;
    private $horarioAtendimentoMedico;
    private $apresentouCopiaAtestado;
    private $funcionario;
    private $testemunha;
    private $comunicado;
    private $perguntasRespondidas;

    public function __construct($id, $dataCriacao, $horaCriacao, $horarioInicioTrabalho, $dataAtendimentoMedico,
            $horarioAtendimentoMedico, $apresentouCopiaAtestado, $funcionario, $testemunha, $comunicado, $perguntasRespondidas=array()) {
        $this->id=$id;
        $this->dataCriacao=$dataCriacao;
        $this->horaCriacao=$horaCriacao;
        $this->dataAtendimentoMedico=$dataAtendimentoMedico;
        $this->horarioInicioTrabalho=$horarioInicioTrabalho;
        $this->horarioAtendimentoMedico=$horarioAtendimentoMedico;
        $this->apresentouCopiaAtestado=$apresentouCopiaAtestado;
        $this->funcionario=$funcionario;
        $this->testemunha=$testemunha;
        $this->comunicado=$comunicado;
        $this->perguntasRespondidas=$perguntasRespondidas;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id){
        $this->id=$id;
    }

    public function getDataCriacao() {
        return $this->dataCriacao;
    }

    public function setDataCriacao($dataCriacao){
        $this->dataCriacao=$dataCriacao;
    }
    public function getHorarioInicioTrabalho() {
        return $this->horarioInicioTrabalho;
    }

    public function setHorarioInicioTrabalho($horarioInicioTrabalho){
        $this->horarioInicioTrabalho=$horarioInicioTrabalho;
    }
    public function getHoraCriacao() {
        return $this->horaCriacao;
    }

    public function setHoraCriacao($horaCriacao){
        $this->horaCriacao=$horaCriacao;
    }
    public function getDataAtendimentoMedico() {
        return $this->dataAtendimentoMedico;
    }

    public function setDataAtendimentoMedico($dataAtendimentoMedico){
        $this->dataAtendimentoMedico=$dataAtendimentoMedico;
    }
    public function getHorarioAtendimentoMedico() {
        return $this->horarioAtendimentoMedico;
    }

    public function setHorarioAtendimentoMedico($horarioAtendimentoMedico){
        $this->horarioAtendimentoMedico=$horarioAtendimentoMedico;
    }
    public function getApresentouCopiaAtestado() {
        return $this->apresentouCopiaAtestado;
    }

    public function setApresentouCopiaAtestado($apresentouCopiaAtestado){
        $this->apresentouCopiaAtestado=$apresentouCopiaAtestado;
    }

    public function getFuncionario() {
        return $this->funcionario;
    }

    public function setFuncionario($funcionario){
        $this->funcionario=$funcionario;
    }
    public function getTestemunha() {
        return $this->testemunha;
    }

    public function setTestemunha($testemunha){
        $this->testemunha=$testemunha;
    }
    public function getComunicado() {
        return $this->comunicado;
    }

    public function setComunicado($comunicado){
        $this->comunicado=$comunicado;
    }
    public function getPerguntasRespondidas() {
        return $this->perguntasRespondidas;
    }

    public function setPerguntasRespondidas($perguntasRespondidas){
        $this->perguntasRespondidas=$perguntasRespondidas;
    }

    public function toArray(){
        $json=array(
            'id'=>  $this->id,
            'dataCriacao'=>  $this->dataCriacao,
            'horaCriacao'=> $this->horaCriacao,
            'horarioInicioTrabalho'=>  $this->horarioInicioTrabalho,
            'dataAtendimentoMedico'=>  $this->dataAtendimentoMedico,
            'horarioAtendimentoMedico'=>  $this->horarioAtendimentoMedico,
            'apresentouCopiaAtestado'=> $this->apresentouCopiaAtestado,
            'funcionario'=>  $this->funcionario,
            'testemunha'=>  $this->testemunha,
            'comunicado'=>  $this->comunicado,
            'perguntasRespondidas'=>  $this->perguntasRespondidas
        );
        return $json;
    }
}
