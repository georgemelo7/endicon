<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of SubCategoriaCausa
 *
 * @author George Tassiano
 */
class SubCategoriaCausa {
    private $id;
    private $nome;
    private $categoriaCausa;
    
    
    public function __construct($id, $nome, $categoriaCausa) {
        $this->id=$id;
        $this->nome=$nome;
        $this->categoriaCausa=$categoriaCausa;
        
    }
    public function getid() {
        return $this->id; 
    }
    
    public function setId($id){
        $this->id=$id;
    }
    public function getNome() {
        return $this->nome; 
    }
    
    public function setNome($nome){
        $this->nome=$nome;
    }
    public function getCategoriaCausa() {
        return $this->categoriaCausa; 
    }
    
    public function setCategoriaCausa($categoriaCausa){
        $this->categoriaCausa=$categoriaCausa;
    }
    
    public function toArray(){
        $json=array(
            'id'=>  $this->id,
            'nome'=>  $this->nome,
            'categoriaCausa'=>  $this->categoriaCausa
        );
        return $json;
    }
}
