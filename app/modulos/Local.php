<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Modulos;
class Local{
    private $id;
    private $endereco;
    private $cep;
    private $posicaoGeografica;
    private $estado;
    private $cidade;
    
    public function __construct($id, $endereco, $cep, $posicaoGeografica, $estado, $cidade) {
        $this->id=$id;
        $this->endereco=$endereco;
        $this->cep=$cep;
        $this->posicaoGeografica=$posicaoGeografica;
        $this->estado=$estado;
        $this->cidade=$cidade;
    }
    
    public function getid() {
        return $this->id; 
    }
    
    public function setId($id){
        $this->id=$id;
    }
    
    public function getEndereco() {
        return $this->endereco; 
    }
    
    public function setEndereco($endereco){
        $this->endereco=$endereco;
    }
    
    public function getCep() {
        return $this->cep; 
    }
    
    public function setCep($cep){
        $this->cep=$cep;
    }
    
    public function getPosicaoGeografica() {
        return $this->posicaoGeografica; 
    }
    
    public function setPosicaoGeografica($posicaoGeografica){
        $this->posicaoGeografica=$posicaoGeografica;
    }
    
    public function getEstado() {
        return $this->estado; 
    }
    
    public function setEstado($estado){
        $this->estado=$estado;
    }
    
    public function getCidade() {
        return $this->cidade; 
    }
    
    public function setCidade($cidade){
        $this->cidade=$cidade;
    }
    
    public function toArray(){
        $json=array(
            'id'=>  $this->id,
            'endereco'=>  $this->endereco,
            'cep'=>  $this->cep,
            'posicaoGeografica'=>  $this->posicaoGeografica,
            'estado'=>  $this->estado,
            'cidade'=>  $this->cidade
        );
        return $json;
    }
    
}