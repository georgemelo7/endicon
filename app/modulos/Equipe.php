<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Equipe
 *
 * @author George Tassiano
 */
class Equipe {
    private $id;
    private $nome;
    private $codigo;
    
    
    public function __construct($id, $nome, $codigo) {
        $this->id=$id;
        $this->nome=$nome;
        $this->codigo=$codigo;
        
    }
    public function getid() {
        return $this->id; 
    }
    
    public function setId($id){
        $this->id=$id;
    }
    public function getNome() {
        return $this->nome; 
    }
    
    public function setNome($nome){
        $this->nome=$nome;
    }
    public function getCodigo() {
        return $this->codigo; 
    }
    
    public function setCodigo($codigo){
        $this->codigo=$codigo;
    }
    
    public function toArray(){
        $json=array(
            'id'=>  $this->id,
            'nome'=>  $this->nome,
            'codigo'=>  $this->codigo
        );
        return $json;
    }
}
