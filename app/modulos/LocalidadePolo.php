<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of LocalidadePolo
 *
 * @author George Tassiano
 */
class LocalidadePolo {
    private $id;
    private $nome;
    private $polo;
    
    public function __construct($id, $nome, $polo) {
        $this->id=$id;
        $this->nome=$nome;
        $this->polo=$polo;
    }
    public function getid() {
        return $this->id; 
    }
    
    public function setId($id){
        $this->id=$id;
    }
    public function getNome() {
        return $this->nome; 
    }
    
    public function setNome($nome){
        $this->nome=$nome;
    }
    public function getPolo() {
        return $this->polo; 
    }
    
    public function setPolo($polo){
        $this->polo=$polo;
    }
    
    public function toArray(){
        $json=array(
            'id'=>  $this->id,
            'nome'=>  $this->nome,
            'polo'=>  $this->polo
        );
        return $json;
    }
}
