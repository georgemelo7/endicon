<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Processo
 *
 * @author George Tassiano
 */
class ConhecimentoRelatorio {
    private $idFuncionario;
    private $idRelatorioInvestigacao;
    private $data;


    public function __construct($idRelatorioInvestigacao, $idFuncionario, $data) {
        $this->idRelatorioInvestigacao=$idRelatorioInvestigacao;
        $this->idFuncionario=$idFuncionario;
        $this->data=$data;

    }
    public function getIdRelatorioInvestigacao() {
        return $this->idRelatorioInvestigacao;
    }

    public function setIdRelatorioInvestigacao($idRelatorioInvestigacao){
        $this->idRelatorioInvestigacao=$idRelatorioInvestigacao;
    }
    public function getIdFuncionario() {
        return $this->idFuncionario;
    }

    public function setIdFuncionario($idFuncionario){
        $this->idFuncionario=$idFuncionario;
    }

    public function getData() {
        return $this->data;
    }

    public function setData($data){
        $this->data=$data;
    }

    public function toArray(){
        $json=array(
            'idRelatorioInvestigacao'=>  $this->idRelatorioInvestigacao,
            'idFuncionario'=> $this->idFuncionario,
            'data'=>  $this->data
        );
        return $json;
    }
}
