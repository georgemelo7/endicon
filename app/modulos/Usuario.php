<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Usuario
 *
 * @author George Tassiano
 */
class Usuario {
    private $id;
    private $senha;
    private $ultimoAcesso;
    private $grupoPermissao;
    private $funcionario;
    
    public function __construct($id, $senha, $ultimoAcesso, $grupoPermissao, $funcionario) {
        $this->id=$id;
        $this->senha=$id;
        $this->ultimoAcesso=$ultimoAcesso;
        $this->grupoPermissao=$grupoPermissao;
        $this->funcionario=$funcionario;
    }
    public function getId() {
        return $this->id; 
    }
    
    public function setId($id){
        $this->id=$id;
    }
    
    public function getSenha(){
        return $this->senha;
    }
    
    public function setSenha($senha){
        $this->senha=$senha;
    }
    
    public function getUltimoAcesso(){
        return $this->ultimoAcesso;
    }
    
    public function setUltimoAcesso($ultimoAcesso){
        $this->ultimoAcesso=$ultimoAcesso;
    }
    
    public function getGrupoPermissao(){
        return $this->grupoPermissao;
    }
    
    public function setGrupoPermissao($grupoPermissao){
        $this->grupoPermissao=$grupoPermissao;
    }
    
    public function getFuncionario() {
        return $this->funcionario; 
    }
    
    public function setFuncionario($funcionario){
        $this->funcionario=$funcionario;
    }
    
    public function toArray(){
        $json = array(
            'id'=>  $this->id,
            'senha'=>  $this->senha,
            'ultimoAcesso'=>  $this->ultimoAcesso,
            'grupoPermissao'=>  $this->grupoPermissao,
            'funcionario'=> $this->funcionario
        );
        return $json;
    }
    
}
