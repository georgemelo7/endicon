<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Processo
 *
 * @author George Tassiano
 */
class FuncionarioComite {
    private $idFuncionario;
    private $idComite;
    private $sigla;


    public function __construct($idFuncionario, $idComite, $sigla) {
        $this->idFuncionario=$idFuncionario;
        $this->idComite=$idComite;
        $this->sigla=$sigla;

    }
    public function getIdFuncionario() {
        return $this->idFuncionario;
    }

    public function setIdFuncionario($idFuncionario){
        $this->idFuncionario=$idFuncionario;
    }
    public function getComite() {
        return $this->comite;
    }

    public function setComite($comite){
        $this->comite=$comite;
    }
    public function getSigla() {
        return $this->sigla;
    }

    public function setSigla($sigla){
        $this->sigla=$sigla;
    }

    public function toArray(){
        $json=array(
            'idFuncionario'=>  $this->idFuncionario,
            'idComite'=> $this->idComite,
            'sigla'=>  $this->sigla
        );
        return $json;
    }
}
