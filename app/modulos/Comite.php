<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Processo
 *
 * @author George Tassiano
 */
class Comite {
    private $id;
    private $data;


    public function __construct($id, $data) {
        $this->id=$id;
        $this->data=$data;

    }
    public function getid() {
        return $this->id;
    }

    public function setId($id){
        $this->id=$id;
    }
    public function getData() {
        return $this->data;
    }

    public function setData($data){
        $this->data=$data;
    }

    public function toArray(){
        $json=array(
            'id'=>  $this->id,
            'data'=>  $this->data
        );
        return $json;
    }
}
