<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Modulos;
class AnaliseRisco{
    private $id;
    private $DataCriacao;
    private $horaCriacao;
    private $descricaoAtividade;
    private $observacao;
    private $local;
    private $funcionario;
    private $tipoProcesso;
    private $perguntasRespondidas;
    private $equipamentosIndividuais;
    private $equipamentosColetivos;
    private $riscosIdentificados;
    private $membros;
    
    public function __construct($id, $dataCriacao, $horaCriacao, $descricaoAtividade, $observacao,
            $local, $funcionario, $tipoProcesso, $perguntasRespondidas=null, $equipamentosIndividuais=null, $equipamentosColetivos=null, $riscosIdentificados=null, $membros=null) {
        $this->id=$id;
        $this->DataCriacao=$dataCriacao;
        $this->horaCriacao=$horaCriacao;
        $this->descricaoAtividade=$descricaoAtividade;
        $this->observacao=$observacao;
        $this->local=$local;
        $this->funcionario=$funcionario;
        $this->tipoProcesso=$tipoProcesso;
        $this->perguntasRespondidas=$perguntasRespondidas;
        $this->equipamentosIndividuais=$equipamentosIndividuais;
        $this->equipamentosColetivos=$equipamentosColetivos;
        $this->riscosIdentificados=$riscosIdentificados;
        $this->membros=$membros;
    }
    
    public function getId() {
        return $this->id; 
    }
    
    public function setId($id){
        $this->id=$id;
    }
    public function getDataCriacao() {
        return $this->DataCriacao; 
    }
    
    public function setDataCriacao($dataCriacao){
        $this->DataCriacao=$dataCriacao;
    }
    public function getHoraCriacao() {
        return $this->horaCriacao; 
    }
    
    public function setHoraCriacao($horaCriacao){
        $this->horaCriacao=$horaCriacao;
    }
    public function getDescricaoAtividade() {
        return $this->descricaoAtividade; 
    }
    
    public function setDescricaoAtividade($descricaoAtividade){
        $this->descricaoAtividade=$descricaoAtividade;
    }
    public function getObservacao() {
        return $this->observacao; 
    }
    
    public function setObservacao($observacao){
        $this->observacao=$observacao;
    }
    public function getLocal() {
        return $this->local; 
    }
    
    public function setLocal($local){
        $this->local=$local;
    }
    public function getFuncionario() {
        return $this->funcionario; 
    }
    
    public function setFuncionario($funcionario){
        $this->funcionario=$funcionario;
    }
    public function getTipoProcesso() {
        return $this->tipoProcesso; 
    }
    
    public function setTipoProcesso($tipoProcesso){
        $this->tipoProcesso=$tipoProcesso;
    }
    public function getPerguntasRespondidas() {
        return $this->perguntasRespondidas; 
    }
    
    public function setPerguntasRespondidas($perguntasRespondidas){
        $this->perguntasRespondidas=$perguntasRespondidas;
    }
    public function getEquipamentosIndividuais() {
        return $this->equipamentosIndividuais; 
    }
    
    public function setEquipamentosIndividuais($equipamentosIndividuais){
        $this->equipamentosIndividuais=$equipamentosIndividuais;
    }
    public function getEquipamentosColetivos() {
        return $this->equipamentosColetivos; 
    }
    
    public function setEquipamentosColetivos($equipamentosColetivos){
        $this->equipamentosColetivos=$equipamentosColetivos;
    }
    public function getRiscosIdentificados() {
        return $this->riscosIdentificados; 
    }
    
    public function setRiscosIdentificados($riscosIdentificados){
        $this->riscosIdentificados=$riscosIdentificados;
    }
    public function getMembros() {
        return $this->membros; 
    }
    
    public function setMembros($membros){
        $this->membros=$membros;
    }
    
    public function toArray(){
        $json = array(
            'id'=> $this->id,
            'dataCriacao'=>  $this->DataCriacao,
            'horaCriacao'=>  $this->horaCriacao,
            'descricaoAtividade'=>  $this->descricaoAtividade,
            'observacao'=>  $this->observacao,
            'local'=> $this->local,
            'funcionario'=> $this->funcionario,
            'tipoProcesso'=>  $this->tipoProcesso,
            'perguntasRespondidas'=>  $this->perguntasRespondidas,
            'equipamentosIndividuais'=>  $this->equipamentosIndividuais,
            'equipamentosColetivos'=> $this->equipamentosColetivos,
            'riscosIdentificados'=> $this->riscosIdentificados,
            'membros'=> $this->membros
        );
        return $json;
    }
            
}