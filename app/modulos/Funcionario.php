<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

class Funcionario {

    private $id;
    private $nome;
    private $matricula;
    private $dataNascimento;
    private $sexo;
    private $estadoCivil;
    private $inicioFuncao;
    private $grauInstrucao;
    private $estadoInstrucao;
    private $inicioRefeicao;
    private $fimRefeicao;
    private $turnoTrabalho;
    private $nacionalidade;
    private $funcao;
    private $local;
    private $localidadePolo;
    private $treinamentosRealizados;

    public function __construct($id, $nome, $matricula, $dataNascimento, $sexo, $estadoCivil, $inicioFuncao, $grauInstrucao, $estadoInstrucao, $inicioRefeicao, $fimRefeicao, $turnoTrabalho, $nacionalidade, $funcao, $local, $localidadePolo, $treinamentosRealizados=null) {
        $this->id = $id;
        $this->nome = $nome;
        $this->matricula = $matricula;
        $this->dataNascimento = $dataNascimento;
        $this->sexo = $sexo;
        $this->estadoCivil = $estadoCivil;
        $this->inicioFuncao = $inicioFuncao;
        $this->grauInstrucao = $grauInstrucao;
        $this->estadoInstrucao = $estadoInstrucao;
        $this->inicioRefeicao = $inicioRefeicao;
        $this->fimRefeicao = $fimRefeicao;
        $this->turnoTrabalho = $turnoTrabalho;
        $this->nacionalidade = $nacionalidade;
        $this->funcao = $funcao;
        $this->local = $local;
        $this->localidadePolo = $localidadePolo;
        $this->treinamentosRealizados = $treinamentosRealizados;
    }

    public function getid() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getNome() {
        return $this->nome;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function getMatricula() {
        return $this->matricula;
    }

    public function setMatricula($matricula) {
        $this->matricula = $matricula;
    }

    public function getDataNascimento() {
        return $this->dataNascimento;
    }

    public function setDataNascimento($dataNascimento) {
        $this->dataNascimento = $dataNascimento;
    }

    public function getSexo() {
        return $this->sexo;
    }

    public function setSexo($sexo) {
        $this->sexo = $sexo;
    }

    public function getEstadoCivil() {
        return $this->estadoCivil;
    }

    public function setEstadoCivil($estadoCivil) {
        $this->estadoCivil = $estadoCivil;
    }

    public function getInicioFuncao() {
        return $this->inicioFuncao;
    }

    public function setInicioFuncao($inicioFuncao) {
        $this->inicioFuncao = $inicioFuncao;
    }

    public function getGrauInstrucao() {
        return $this->grauInstrucao;
    }

    public function setGrauInstrucao($grauInstrucao) {
        $this->grauInstrucao = $grauInstrucao;
    }

    public function getEstadoInstrucao() {
        return $this->estadoInstrucao;
    }

    public function setEstadoInstrucao($estadoInstrucao) {
        $this->estadoInstrucao = $estadoInstrucao;
    }

    public function getInicioRefeicao() {
        return $this->inicioRefeicao;
    }

    public function setInicioRefeicao($inicioRefeicao) {
        $this->inicioRefeicao = $inicioRefeicao;
    }

    public function getFimRefeicao() {
        return $this->fimRefeicao;
    }

    public function setFimRefeicao($fimRefeicao) {
        $this->fimRefeicao = $fimRefeicao;
    }

    public function getTurnoTrabalho() {
        return $this->inicioRefeicao;
    }

    public function setTurnoTrabalho($turnoTrabalho) {
        $this->turnoTrabalho = $turnoTrabalho;
    }

    public function getNacionalidade() {
        return $this->nacionalidade;
    }

    public function setNacionalidade($nacionalidade) {
        $this->nacionalidade = $nacionalidade;
    }

    public function getFuncao() {
        return $this->funcao;
    }

    public function setFuncao($funcao) {
        $this->funcao = $funcao;
    }

    public function getLocal() {
        return $this->local;
    }

    public function setLocal($local) {
        $this->local = $local;
    }

    public function getLocalidadePolo() {
        return $this->localidadePolo;
    }

    public function setLocalidadePolo($localidadePolo) {
        $this->localidadePolo = $localidadePolo;
    }
    public function getTreinamentosRealizados() {
        return $this->treinamentosRealizados;
    }

    public function setTreinamentosRealizados($treinamentosRealizados) {
        $this->treinamentosRealizados = $treinamentosRealizados;
    }

    public function toArray() {
        $json = array(
            'id' => $this->id,
            'nome' => $this->nome,
            'matricula' => $this->matricula,
            'dataNascimento' => $this->dataNascimento,
            'sexo' => $this->sexo,
            'estadoCivil' => $this->estadoCivil,
            'inicioFuncao' => $this->inicioFuncao,
            'grauInstrucao' => $this->grauInstrucao,
            'estadoInstrucao' => $this->estadoInstrucao,
            'inicioRefeicao' => $this->inicioRefeicao,
            'fimRefeicao' => $this->fimRefeicao,
            'turnoTrabalho' => $this->turnoTrabalho,
            'nacionalidade' => $this->nacionalidade,
            'funcao' => $this->funcao,
            'local' => $this->local,
            'localidadePolo' => $this->localidadePolo,
            'treinamentosRealizados'=>  $this->treinamentosRealizados
        );
        return $json;
    }

}
