<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of DiaAtestado
 *
 * @author George Tassiano
 */
class Atestado {
    private $id;
    private $inicioAtestado;
    private $fimAtestado;


    public function __construct($id, $inicioAtestado, $fimAtestado) {
        $this->id=$id;
        $this->inicioAtestado=$inicioAtestado;
        $this->fimAtestado=$fimAtestado;

    }
    public function getid() {
        return $this->id;
    }

    public function setId($id){
        $this->id=$id;
    }
    public function getInicioAtestado() {
        return $this->inicioAtestado;
    }

    public function setInicioAtestado($inicioAtestado){
        $this->inicioAtestado=$inicioAtestado;
    }
    public function getFimAtestado() {
        return $this->fimAtestado;
    }

    public function setFimAtestado($fimAtestado){
        $this->fimAtestado=$fimAtestado;
    }

    public function toArray(){
        $json=array(
            'id'=>  $this->id,
            'inicioAtestado'=>  $this->inicioAtestado,
            'fimAtestado'=>  $this->fimAtestado
        );
        return $json;
    }
}
