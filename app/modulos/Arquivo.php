<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Arquivo
 *
 * @author George Tassiano
 */
class Arquivo {
    private $id;
    private $nome;
    private $tipo;
    private $descricao;
    private $tamanhoArquivo;
    private $pasta;
    private $usuario;
    
    
    public function __construct($id, $nome, $tipo, $descricao, $tamanhoArquivo, $pasta, $usuario) {
        $this->id=$id;
        $this->nome=$nome;
        $this->tipo=$tipo;
        $this->descricao=$descricao;
        $this->tamanhoArquivo=$tamanhoArquivo;
        $this->pasta=$pasta;
        $this->usuario=$usuario;
    }
    public function getid() {
        return $this->id; 
    }
    
    public function setId($id){
        $this->id=$id;
    }
    public function getNome() {
        return $this->nome; 
    }
    
    public function setNome($nome){
        $this->nome=$nome;
    }
    public function getTipo() {
        return $this->tipo; 
    }
    
    public function setTipo($tipo){
        $this->tipo=$tipo;
    }
    
    public function getDescricao() {
        return $this->descricao; 
    }
    
    public function setDescricao($descricao){
        $this->descricao=$descricao;
    }
    
    public function getTamanhoArquivo() {
        return $this->tamanhoArquivo; 
    }
    
    public function setTamanhoArquivo($tamanhoArquivo){
        $this->tamanhoArquivo=$tamanhoArquivo;
    }
    
    public function getPasta() {
        return $this->pasta; 
    }
    
    public function setPasta($pasta){
        $this->pasta=$pasta;
    }
    
    public function getUsuario() {
        return $this->usuario; 
    }
    
    public function setUsuario($usuario){
        $this->usuario=$usuario;
    }
    
    public function toArray(){
        $json=array(
            'id'=>  $this->id,
            'nome'=>  $this->nome,
            'tipo'=>  $this->tipo,
            'descricao'=>  $this->descricao,
            'tamanhoArquivo'=>  $this->tamanhoArquivo,
            'pasta'=>  $this->pasta,
            'usuario'=>  $this->usuario
        );
        return $json;
    }
}
