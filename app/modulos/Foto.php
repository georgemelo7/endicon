<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Modulos;
class Foto{
    private $id;
    private $desvio;
    private $foto;
    private $tamanhoImagem;
    private $descricao;
    private $tipoImagem;
    private $nomeImagem;
    
    public function __construct($id, $foto, $tamanhoImagem, $descricao, $tipoImagem, $nomeImagem, $desvio) {
        $this->id=$id;
        $this->desvio=$desvio;
        $this->foto=$foto;
        $this->tamanhoImagem=$tamanhoImagem;
        $this->descricao=$descricao;
        $this->tipoImagem=$tipoImagem;
        $this->nomeImagem=$nomeImagem;
    }
    
    public function getid() {
        return $this->id; 
    }
    
    public function setId($id){
        $this->id=$id;
    }
    public function getDesvio() {
        return $this->desvio; 
    }
    
    public function setDesvio($desvio){
        $this->desvio=$desvio;
    }
    public function getFoto() {
        return $this->foto; 
    }
    
    public function setfoto($foto){
        $this->foto=$foto;
    }
    public function getTamanhoImagem() {
        return $this->tamanhoImagem; 
    }
    
    public function setTamanhoImagem($tamanhoImagem){
        $this->tamanhoImagem=$tamanhoImagem;
    }
    public function getDescricao() {
        return $this->descricao; 
    }
    
    public function setDescricao($descricao){
        $this->descricao=$descricao;
    }
    public function getTipoImagem() {
        return $this->tipoImagem; 
    }
    
    public function setTipoImagem($tipoImagem){
        $this->tipoImagem=$tipoImagem;
    }
    public function getNomeImagem() {
        return $this->nomeImagem; 
    }
    
    public function setNomeImagem($nomeImagem){
        $this->nomeImagem=$nomeImagem;
    }
    
    public function toArray(){
        $json = array(
            'id'=>  $this->id,
            'foto'=>  $this->foto,
            'tamanhoImagem'=>  $this->tamanhoImagem,
            'descricao'=>  $this->descricao,
            'tipoImagem'=>  $this->tipoImagem,
            'nomeImagem'=>  $this->nomeImagem,
            'desvio'=>  $this->desvio
        );
        return $json;
    }
}

