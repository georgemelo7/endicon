<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of Processo
 *
 * @author George Tassiano
 */
class AcaoCorretiva {
    private $id;
    private $idFuncionario;
    private $idRelatorioInvestigacao;
    private $data;
    private $acao;


    public function __construct($id, $idRelatorioInvestigacao, $idFuncionario, $data, $acao) {
        $this->id= $id;
        $this->idRelatorioInvestigacao=$idRelatorioInvestigacao;
        $this->idFuncionario=$idFuncionario;
        $this->data=$data;
        $this->acao=$acao;
    }
    public function getId() {
        return $this->id;
    }

    public function setId($id){
        $this->id=$id;
    }
    public function getIdRelatorioInvestigacao() {
        return $this->idRelatorioInvestigacao;
    }

    public function setIdRelatorioInvestigacao($idRelatorioInvestigacao){
        $this->idRelatorioInvestigacao=$idRelatorioInvestigacao;
    }
    public function getIdFuncionario() {
        return $this->idFuncionario;
    }

    public function setIdFuncionario($idFuncionario){
        $this->idFuncionario=$idFuncionario;
    }

    public function getData() {
        return $this->data;
    }

    public function setData($data){
        $this->data=$data;
    }
    public function getAcao() {
        return $this->acao;
    }

    public function setAcao($acao){
        $this->acao=$acao;
    }

    public function toArray(){
        $json=array(
            'idRelatorioInvestigacao'=>  $this->idRelatorioInvestigacao,
            'idFuncionario'=> $this->idFuncionario,
            'data'=>  $this->data,
            'acao'=> $this->acao
        );
        return $json;
    }
}
