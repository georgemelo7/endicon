<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modulos;

/**
 * Description of TreinamentoRealizado
 *
 * @author George Tassiano
 */
class TreinamentoRealizado {
    private $idFuncionario;
    private $idTreinamento;
    private $data;
    private $instrutor;
    private $cargaHoraria;


    public function __construct($idFuncionario, $idTreinamento, $data, $instrutor, $cargaHoraria) {
        $this->idFuncionario=$idFuncionario;
        $this->idTreinamento=$idTreinamento;
        $this->data=$data;
        $this->instrutor=$instrutor;
        $this->cargaHoraria=$cargaHoraria;
    }
    public function getIdFuncionario() {
        return $this->idFuncionario;
    }

    public function setIdFuncionario($idFuncionario){
        $this->idFuncionario=$idFuncionario;
    }
    public function getIdTreinamento() {
        return $this->idTreinamento;
    }

    public function setIdTreinamento($idTreinamento){
        $this->idTreinamento=$idTreinamento;
    }
    public function getData() {
        return $this->data;
    }

    public function setData($data){
        $this->data=$data;
    }
    public function getInstrutor() {
        return $this->instrutor;
    }

    public function setInstrutor($instrutor){
        $this->instrutor=$instrutor;
    }
    public function getCargaHoraria() {
        return $this->cargaHoraria;
    }

    public function setCargaHoraria($cargaHoraria){
        $this->cargaHoraria=$cargaHoraria;
    }

    public function toArray(){
        $json=array(
            'idFuncionario'=>  $this->idFuncionario,
            'idTreinamento'=>  $this->idTreinamento,
            'data'=>  $this->data,
            'instrutor'=>  $this->instrutor,
            'cargaHoraria'=> $this->cargaHoraria
        );
        return $json;
    }
}
