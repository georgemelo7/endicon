<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerEquipamentosIndividuaisAnaliseRisco
 *
 * @author ADM
 */
use App\Modulos\BD;
class ControllerEquipamentoIndividualAnaliseRisco {
    public function deleteEquipamentosIndividuaisAnaliseRisco($referencia){
        $bd = new BD();
        $sql = "DELETE FROM TB_AnaliseRisco_has_TB_EquipamentoIndividual WHERE TB_AnaliseRisco_idTB_AnaliseRisco = :idAnaliseRisco";
        $bd->query($sql);
        $bd->bind(':idAnaliseRisco', $referencia);
        $bd->execute();
        $bd->close();
    }
    
    public function postEquipamentoIndividualAnaliseRisco($referencia, $equipamentoIndividual){
        $bd = new BD();
        $sql = "INSERT INTO TB_AnaliseRisco_has_TB_EquipamentoIndividual (TB_AnaliseRisco_idTB_AnaliseRisco, TB_EquipamentoIndividual_idTB_EquipamentoIndividual) VALUES (:idAnaliseRisco, :idEquipamentoIndividual)";
        $bd->query($sql);
        $bd->bind(':idAnaliseRisco', $referencia);
        $bd->bind(':idEquipamentoIndividual', $equipamentoIndividual);
        $bd->execute();
        $json=array(
            'id'=>(int)$bd->lastInput()
        );
        $bd->close();
        return $json;
    }
}
