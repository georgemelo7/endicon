<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ConstrollerTreinamento
 *
 * @author George Tassiano
 */
use App\Modulos\BD;
use App\Modulos\Treinamento;
class ControllerTreinamento {
        public function getTreinamento($idTreinamento){

        $bd = new BD();
        $sql = "SELECT * FROM TB_Treinamento WHERE idTB_Treinamento = :idTreinamento";
        $bd->query($sql);
        $bd->bind(':idTreinamento', $idTreinamento);
        $bd->execute();
        $row=$bd->single();
        if(!empty($row)){
            $tre = new Treinamento($row["idTB_Treinamento"], $row["Nome"], $row["Codigo"], $row["ConteudoProgramatico"]);
            $treinamento=$tre->toArray();
        }else{
            $treinamento=null;
        }
        $bd->close();
        return $treinamento;
    }

    public function getTreinamentos() {
        $bd = new BD();
        $sql = "SELECT * FROM TB_Treinamento";
        $bd->query($sql);
        if ($bd->execute()) {
            $treinamentos=array();
            while ($row = $bd->single()) {
                $tre = new Treinamento($row["idTB_Treinamento"], $row["Nome"], $row["Codigo"], $row["ConteudoProgramatico"]);
                $treinamentos[] = $tre->toArray();
            }
        }else{
            $treinamentos=null;
        }
        $bd->close();
        return $treinamentos;
    }
    public function deleteTreinamento($idTreinamento){
        $bd = new BD();
        $sql = "DELETE FROM TB_Treinamento WHERE idTB_Treinamento = :idTreinamento";
        $bd->query($sql);
        $bd->bind(':idTreinamento', $idTreinamento);
        $bd->execute();
        $bd->close();
    }

    public function postTreinamento($dados){
        $bd = new BD();
        $sql = "INSERT INTO TB_Treinamento (Nome, Codigo, ConteudoProgramatico) VALUES (:nome, :codigo, :conteudoProgramatico)";
        $bd->query($sql);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->bind(':codigo', $dados["Codigo"]);
        $bd->bind(':conteudoProgramatico', $dados["ConteudoProgramatico"]);
        $bd->execute();
         $json=array(
            'id'=>(int)$bd->lastInput()
        );
        $bd->close();
        return $json;
    }

    public function putTreinamento($idTreinamento, $dados){
        $bd = new BD();
        $sql = "UPDATE TB_Treinamento SET Nome=:nome, Codigo=:codigo, ConteudoProgramatico=:conteudoProgramatico WHERE idTB_Treinamento = :idTreinamento";
        $bd->query($sql);
        $bd->bind(':idTreinamento', $idTreinamento);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->bind(':codigo', $dados["Codigo"]);
        $bd->bind(':conteudoProgramatico', $dados["ConteudoProgramatico"]);
        $bd->execute();
        $bd->close();
    }
}
