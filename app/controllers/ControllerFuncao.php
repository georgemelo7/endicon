<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

use App\Modulos\BD;
use App\Modulos\Funcao;

/**
 * Description of ControllerFuncao
 *
 * @author George Tassiano
 */
class ControllerFuncao {

    public function getFuncao($idFuncao) {

        $bd = new BD();
        $sql = "SELECT * FROM TB_Funcao WHERE idTB_Funcao = :idFuncao";
        $bd->query($sql);
        $bd->bind(':idFuncao', $idFuncao);
        $bd->execute();
        $row = $bd->single();
        if (!empty($row)) {
            $fun = new Funcao($row["idTB_Funcao"], $row["Nome"]);
            $funcao = $fun->toArray();
        } else {
            $funcao = null;
        }
        $bd->close();
        return $funcao;
    }

    public function getFuncoes() {
        $bd = new BD();
        $sql = "SELECT * FROM TB_Funcao";
        $bd->query($sql);
        if ($bd->execute()) {
            $funcoes=array();
            while ($row = $bd->single()) {
                $fun = new Funcao($row["idTB_Funcao"], $row["Nome"]);
                $funcoes[] = $fun->toArray();
            }
        }else{
            $funcoes=null;
        }
        $bd->close();
        return $funcoes;
    }
    public function deleteFuncao($idFuncao){
        $bd = new BD();
        $sql = "DELETE FROM TB_Funcao WHERE idTB_Funcao = :idFuncao";
        $bd->query($sql);
        $bd->bind(':idFuncao', $idFuncao);
        $bd->execute();
        $bd->close();
    }
    
    public function postFuncao($dados){
        $bd = new BD();
        $sql = "INSERT INTO TB_Funcao (Nome) VALUES (:nome)";
        $bd->query($sql);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->execute();
         $json=array(
            'id'=>(int)$bd->lastInput()
        );
         $bd->close();
        return $json;
    }
    
    public function putFuncao($idFuncao, $dados){
        $bd = new BD();
        $sql = "UPDATE TB_Funcao SET Nome=:nome WHERE idTB_Funcao = :idFuncao";
        $bd->query($sql);
        $bd->bind(':idFuncao', $idFuncao);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->execute();
        $bd->close();
    }

}
