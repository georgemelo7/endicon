<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerPolo
 *
 * @author George Tassiano
 */
use App\Modulos\BD;
use App\Modulos\Polo;

class ControllerPolo {

    public function getPolo($idPolo) {

        $bd = new BD();
        $sql = "SELECT * FROM TB_Polo WHERE idTB_Polo = :idPolo";
        $bd->query($sql);
        $bd->bind(':idPolo', $idPolo);
        $bd->execute();
        $row = $bd->single();
        if (!empty($row)) {
            $pol = new Polo($row["idTB_Polo"], $row["Nome"], $row["TB_Regional_idTB_Regional"]);
            //recupera Regional
            $controlRegional = new ControllerRegional();
            $pol->setRegional($controlRegional->getRegional($pol->getRegional()));

            $polo = $pol->toArray();
        } else {
            $polo = null;
        }

        $bd->close();
        return $polo;
    }

    public function getPolos() {
        $bd = new BD();
        $sql = "SELECT * FROM TB_Polo";
        $bd->query($sql);
        if ($bd->execute()) {
            $polos = array();
            while ($row = $bd->single()) {
                $pol = new Polo($row["idTB_Polo"], $row["Nome"], $row["TB_Regional_idTB_Regional"]);
                //recupera Regional
                $controlRegional = new ControllerRegional();
                $pol->setRegional($controlRegional->getRegional($pol->getRegional()));
                $polos[] = $pol->toArray();
            }
        } else {
            $polos = null;
        }
        $bd->close();
        return $polos;
    }

    public function deletePolo($idPolo) {
        $bd = new BD();
        $sql = "DELETE FROM TB_Polo WHERE idTB_Polo = :idPolo";
        $bd->query($sql);
        $bd->bind(':idPolo', $idPolo);
        $bd->execute();
        $bd->close();
    }

    public function postPolo($dados) {
        $bd = new BD();
        $sql = "INSERT INTO TB_Polo (Nome, TB_Regional_idTB_Regional) VALUES (:nome, :regional)";
        $bd->query($sql);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->bind(':regional', $dados["TB_Regional_idTB_Regional"]);
        $bd->execute();
         $json=array(
            'id'=>(int)$bd->lastInput()
        );
         $bd->close();
        return $json;
    }

    public function putPolo($idPolo, $dados) {
        $bd = new BD();
        $sql = "UPDATE TB_Polo SET Nome=:nome, TB_Regional_idTB_Regional=:regional WHERE idTB_Polo = :idPolo";
        $bd->query($sql);
        $bd->bind(':idPolo', $idPolo);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->bind(':regional', $dados["TB_Regional_idTB_Regional"]);
        $bd->execute();
        $bd->close();
    }

}
