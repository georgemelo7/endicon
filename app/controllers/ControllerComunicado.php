<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of controller_comunicado
 *
 * @author George Tassiano
 */
use App\Modulos\Comunicado;
use App\Modulos\BD;

class ControllerComunicado {

    public function getComunicados() {
        $bd1 = new BD();
        $sql = "SELECT * FROM TB_ComunicadoAcidente";
        $bd1->query($sql);

        if ($bd1->execute()) {
            $comunicados = array();
            while ($row = $bd1->single()) {
                $comu = new Comunicado($row["idTB_ComunicadoAcidente"], $row["Tipo"], $row["DataCriacao"], $row["HoraCriacao"], $row["Tema"], $row["Descricao"], $row["DataOcorrido"], $row["HoraOcorrido"], $row["DanosMateriais"], $row["DanosAmbientais"], $row["TB_Local_idTB_Local"], $row["TB_Funcionario_idTB_Funcionario"], $row["TB_TipoProcesso_idTB_TipoProcesso"]);


                //recupera os residuos do comunicado
                $controlResiduo = new ControllerResiduo();
                $bd = new BD();
                $sql = "SELECT * FROM TB_Residuo WHERE TB_ComunicadoAcidente_idTB_ComunicadoAcidente=:idComunicado";
                $bd->query($sql);
                $bd->bind(':idComunicado', $comu->getId());
                if ($bd->execute()) {
                    $residuos = array();
                    while ($row = $bd->single()) {
                        $residuos[] = $controlResiduo->getResiduo($row["TB_Residuo_idTB_Residuo"]);
                    }
                } else {
                    $residuos = null;
                }
                $comu->setResiduos($residuos);
                $bd->close();
                $controlFuncionario = new ControllerFuncionario();
                //recupera o funcionario que preencheu o comunicado
                $comu->setFuncionario($controlFuncionario->getFuncionario($comu->getFuncionario()));
                //recupera os funcionarios envolvidos no comunicado
                $bd = new BD();
                $sql = "SELECT * FROM TB_Funcionario_has_TB_ComunicadoAcidente WHERE TB_ComunicadoAcidente_idTB_ComunicadoAcidente=:idComunicado";
                $bd->query($sql);
                $bd->bind(':idComunicado', $comu->getId());
                if ($bd->execute()) {
                    $envolvidos = array();
                    while ($row = $bd->single()) {
                        $envolvidos[] = $controlFuncionario->getFuncionario($row["TB_Funcionario_idTB_Funcionario"]);
                    }
                } else {
                    $envolvidos = null;
                }
                $comu->setEnvolvidos($envolvidos);
                $bd->close();
                //recupera o local do ocorrido
                $controlLocal = new ControllerLocal();
                $comu->setLocal($controlLocal->getLocal($comu->getLocal()));
                //recupera o tipoProcesso
                $controlTipoProcesso = new ControllerTipoProcesso();
                $comu->setTipoProcesso($controlTipoProcesso->getTipoProcesso($comu->getTipoProcesso()));
                //recupera a analiserisco
                $controlAnaliseRisco = new ControllerAnaliseRisco();
                $comu->setAnaliseRisco($controlAnaliseRisco->getAnaliseRisco($comu->getAnaliseRisco()));
                
                $comunicados[] = $comu->toArray();
            }
        } else {
            $comunicados = null;
        }

        $bd1->close();



        return $comunicados;
    }

    public function getComunicado($idComunicado) {
        $bd = new BD();
        $sql = "SELECT * FROM TB_Comunicado WHERE idTB_Comunicado=:idComunicado";
        $bd->query($sql);
        $bd->execute();
        $row = $bd->single();
        $bd->close();
        if (!empty($row)) {
            $com = new Comunicado($row["idTB_ComunicadoAcidente"], $row["Tipo"], $row["DataCriacao"], $row["HoraCriacao"], $row["Tema"], $row["Descricao"], $row["DataOcorrido"], $row["HoraOcorrido"], $row["DanosMateriais"], $row["DanosAmbientais"], $row["TB_Local_idTB_Local"], $row["TB_Funcionario_idTB_Funcionario"], $row["TB_TipoProcesso_idTB_TipoProcesso"]);
            //recupera os residuos do comunicado
            $controlResiduo = new ControllerResiduo();
            $bd = new BD();
            $sql = "SELECT * FROM TB_Residuo WHERE TB_ComunicadoAcidente_idTB_ComunicadoAcidente=:idComunicado";
            $bd->query($sql);
            $bd->bind(':idComunicado', $com->getId());
            if ($bd->execute()) {
                $residuos = array();
                while ($row = $bd->single()) {
                    $residuos[] = $controlResiduo->getResiduo($row["TB_Residuo_idTB_Residuo"]);
                }
            } else {
                $residuos = null;
            }
            $com->setResiduos($residuos);
            $bd->close();

            $controlFuncionario = new ControllerFuncionario();
            //recupera o funcionario que preencheu o comunicado
            $com->setFuncionario($controlFuncionario->getFuncionario($com->getFuncionario()));
            //recupera os funcionarios envolvidos no comunicado
            $bd = new BD();
            $sql = "SELECT * FROM TB_Funcionario_has_TB_ComunicadoAcidente WHERE TB_ComunicadoAcidente_idTB_ComunicadoAcidente=:idComunicado";
            $bd->query($sql);
            $bd->bind(':idComunicado', $com->getId());
            if ($bd->execute()) {
                $envolvidos = array();
                while ($row = $bd->single()) {
                    $envolvidos[] = $controlFuncionario->getFuncionario($row["TB_Funcionario_idTB_Funcionario"]);
                }
            } else {
                $envolvidos = null;
            }
            $com->setEnvolvidos($envolvidos);
            //recupera o local do ocorrido
            $controlLocal = new ControllerLocal();
            $com->setLocal($controlLocal->getLocal($com->getLocal()));
            //recupera o tipoProcesso
            $controlTipoProcesso = new ControllerTipoProcesso();
            $com->setTipoProcesso($controlTipoProcesso->getTipoProcesso($com->getTipoProcesso()));
            //recupera a analiserisco
            $controlAnaliseRisco = new ControllerAnaliseRisco();
            $com->setAnaliseRisco($controlAnaliseRisco->getAnaliseRisco($com->getAnaliseRisco()));
            
            $bd->close();
            $comunicado = $com->toArray();
        } else {
            $comunicado = null;
        }
        return $comunicado;
    }

    public function deleteComunicado($idComunicado) {
        $bd = new BD();
        $sql = "DELETE FROM TB_Comunicado WHERE idTB_Comunicado = :idComunicado";
        $bd->query($sql);
        $bd->bind(':idComunicado', $idComunicado);
        $bd->execute();
        $bd->close();
    }

    public function postComunicado($dados) {
        $bd = new BD();
        $sql = "INSERT INTO TB_Comunicado (HoraCriacao, DataCriacao, Tema, Descricao, DataOcorrido, HoraOcorrido, DanosMateriais, DanosAmbientais, TB_Local_idTB_Local, TB_Funcionario_idTB_Funcionario, TB_TipoProcesso_idTB_TipoProcesso, Tipo, TB_AnaliseRisco_idTB_Analise_Risco) VALUES (:horaCriacao, :dataCriacao, :tema, :descricao, :dataOcorrido, :horaOcorrido, :danosMateriais, :danosAmbientais, :local, :funcionario, :tipoProcesso, :tipo, :analiseRisco)";
        $bd->query($sql);
        $bd->bind(':horaCriacao', $dados["HoraCriacao"]);
        $bd->bind(':dataCriacao', $dados["DataCriacao"]);
        $bd->bind(':tema', $dados["Tema"]);
        $bd->bind(':descricao', $dados["Descricao"]);
        $bd->bind(':dataOcorrido', $dados["DataOcorrido"]);
        $bd->bind(':horaOcorrido', $dados["HoraOcorrido"]);
        $bd->bind(':danosMateriais', $dados["DanosMateriais"]);
        $bd->bind(':danosAmbientais', $dados["DanosAmbientais"]);
        $bd->bind(':local', $dados["TB_Local_idTB_Local"]);
        $bd->bind(':funcionario', $dados["TB_Funcionario_idTB_Funcionario"]);
        $bd->bind(':tipoProcesso', $dados["TB_TipoProcesso_idTB_TipoProcesso"]);
        $bd->bind(':tipo', $dados["Tipo"]);
        $bd->bind(':analiseRisco', $dados["TB_AnaliseRisco_idTB_AnaliseRisco"]);
        $bd->execute();
         $json=array(
            'id'=>(int)$bd->lastInput()
        );
         $bd->close();
        return $json;
    }

    public function putComunicado($idComunicado, $dados) {
        $bd = new BD();
        $sql = "UPDATE TB_Comunicado SET HoraCriacao=:horaCriacao, DataCriacao=:dataCriacao, Tema=:tema, Descricao=:descricao, DataOcorrido=:dataOcorrido, HoraOcorrido=:horaOcorrido, DanosMateriais=:danosMateriais, DanosAmbientais=:danosAmbientais, TB_Local_idTB_Local=:local, TB_Funcionario_idTB_Funcionario=:funcionario, TB_TipoProcesso_idTB_TipoProcesso=:tipoProcesso, Tipo=:tipo, TB_AnaliseRisco_idTB_AnaliseRisco=:analiseRisco WHERE idTB_Comunicado = :idComunicado";
        $bd->query($sql);
        $bd->bind(':idComunicado', $idComunicado);
        $bd->bind(':horaCriacao', $dados["HoraCriacao"]);
        $bd->bind(':dataCriacao', $dados["DataCriacao"]);
        $bd->bind(':tema', $dados["Tema"]);
        $bd->bind(':descricao', $dados["Descricao"]);
        $bd->bind(':dataOcorrido', $dados["DataOcorrido"]);
        $bd->bind(':horaOcorrido', $dados["HoraOcorrido"]);
        $bd->bind(':danosMateriais', $dados["DanosMateriais"]);
        $bd->bind(':danosAmbientais', $dados["DanosAmbientais"]);
        $bd->bind(':local', $dados["TB_Local_idTB_Local"]);
        $bd->bind(':funcionario', $dados["TB_Funcionario_idTB_Funcionario"]);
        $bd->bind(':tipoProcesso', $dados["TB_TipoProcesso_idTB_TipoProcesso"]);
        $bd->bind(':tipo', $dados["Tipo"]);
        $bd->bind(':analiseRisco', $dados["TB_AnaliseRisco_idTB_AnaliseRisco"]);
        $bd->execute();
        $bd->close();
    }

}
