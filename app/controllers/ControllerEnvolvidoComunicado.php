<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerEnvolvido
 *
 * @author ADM
 */
use App\Modulos\Envolvido;
use App\Modulos\BD;

class ControllerEnvolvidoComunicado {

    public function deleteEnvolvidoComunicado($idReferencia, $idFuncionario) {
        $bd = new BD();
        $sql = "DELETE FROM TB_Funcionario_has_TB_ComunicadoAcidente WHERE TB_Funcionario_idTB_Funcionario = :idFuncionario AND TB_ComunicadoAcidente_idTB_ComunicadoAcidente = :idReferencia" ;
        $bd->query($sql);
        $bd->bind(':idReferencia', $idReferencia);
        $bd->bind(':idFuncionario', $idFuncionario);
        $bd->execute();
        $bd->close();
    }

    public function postEnvolvidoComunicado($idReferencia, $idFuncionario) {
        $bd = new BD();
        $sql = "INSERT INTO TB_Funcionario_has_TB_ComunicadoAcidente (TB_Funcionario_idTB_Funcionario, TB_ComunicadoAcidente_idTB_ComunicadoAcidente) VALUES (:idFuncionario, :idReferencia)";
        $bd->query($sql);
        $bd->bind(':idReferencia', $idReferencia);
        $bd->bind(':idFuncionario', $idFuncionario);
        $bd->execute();
         $json=array(
            'id'=>(int)$bd->lastInput()
        );
         $bd->close();
        return $json;
    }
}
