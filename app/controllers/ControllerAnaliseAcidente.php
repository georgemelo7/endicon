<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerAnaliseAcidente
 *
 * @author ADM
 */
use App\Modulos\AnaliseAcidente;
use App\Modulos\BD;

class ControllerAnaliseAcidente {

    public function getAnaliseAcidente($idAnaliseAcidente) {

        $bd = new BD();
        $sql = "SELECT * FROM TB_AnaliseAcidente WHERE idTB_AnaliseAcidente = :idAnaliseAcidente";
        $bd->query($sql);
        $bd->bind(':idAnaliseAcidente', $idAnaliseAcidente);
        $bd->execute();
        $row = $bd->single();
        if (!empty($row)) {
            $ana = new AnaliseAcidente($row['idTB_AnaliseAcidente'], $row['Codigo'], $row['Descricao']);
            $analiseAcidente = $ana->toArray();
        } else {
            $analiseAcidente = null;
        }
        $bd->close();
        return $analiseAcidente;
    }

    public function getAnalises() {
        $bd = new BD();
        $sql = "SELECT * FROM TB_AnaliseAcidente";
        $bd->query($sql);
        if ($bd->execute()) {
            $analisesAcidente=array();
            while ($row = $bd->single()) {
                $ana = new AnaliseAcidente($row['idTB_AnaliseAcidente'], $row['Codigo'], $row['Descricao']);
                $analisesAcidente[] = $ana->toArray();
            }
        }else{
            $analisesAcidente=null;
        }
        $bd->close();
        return $analisesAcidente;
    }
    public function deleteAnaliseAcidente($idAnaliseAcidente){
        $bd = new BD();
        $sql = "DELETE FROM TB_AnaliseAcidente WHERE idTB_AnaliseAcidente = :idAnaliseAcidente";
        $bd->query($sql);
        $bd->bind(':idAnaliseAcidente', $idAnaliseAcidente);
        $bd->execute();
        $bd->close();
    }

    public function postAnaliseAcidente($dados){
        $bd = new BD();
        $sql = "INSERT INTO TB_AnaliseAcidente (Codigo, Descricao) VALUES (:codigo, :descricao)";
        $bd->query($sql);
        $bd->bind(':codigo', $dados["Codigo"]);
        $bd->bind(':descricao', $dados["Descricao"]);
        $bd->execute();
         $json=array(
            'id'=>(int)$bd->lastInput()
        );
         $bd->close();
        return $json;
    }

    public function putAnaliseAcidente($idAnaliseAcidente, $dados){
        $bd = new BD();
        $sql = "UPDATE TB_AnaliseAcidente SET Codigo=:codigo, Descricao=:descricao WHERE idTB_AnaliseAcidente = :idAnaliseAcidente";
        $bd->query($sql);
        $bd->bind(':idAnaliseAcidente', $idAnaliseAcidente);
        $bd->bind(':codigo', $dados["Codigo"]);
        $bd->bind(':descricao', $dados["Descricao"]);
        $bd->execute();
        $bd->close();
    }
}
