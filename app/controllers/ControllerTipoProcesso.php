<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerTipoProcesso
 *
 * @author George Tassiano
 */
use App\Modulos\BD;
use App\Modulos\TipoProcesso;

class ControllerTipoProcesso {

    public function getTipoProcesso($idTipoProcesso) {

        $bd = new BD();
        $sql = "SELECT * FROM TB_TipoProcesso WHERE idTB_TipoProcesso = :idTipoProcesso";
        $bd->query($sql);
        $bd->bind(':idTipoProcesso', $idTipoProcesso);
        $bd->execute();
        $row = $bd->single();
        if (!empty($row)) {
            $tip = new TipoProcesso($row["idTB_TipoProcesso"], $row["Nome"], $row["TB_Processo_idTB_Processo"], $row["TB_Regional_idTB_Regional"]);
            //recuperar o processo
            $controlProcesso = new ControllerProcesso();
            $tip->setProcesso($controlProcesso->getProcesso($tip->getProcesso()));
            //recuperar a regional
            $controlRegional = new ControllerRegional();
            $tip->setRegional($controlRegional->getRegional($tip->getRegional()));

            $tipoProcesso = $tip->toArray();
        } else {
            $tipoProcesso = array();
        }

        $bd->close();
        return $tipoProcesso;
    }

    public function getTiposProcesso() {
        $bd = new BD();
        $sql = "SELECT * FROM TB_TipoProcesso";
        $bd->query($sql);
        if ($bd->execute()) {
            $tiposProcesso = array();
            while ($row = $bd->single()) {
                $tip = new TipoProcesso($row["idTB_TipoProcesso"], $row["Nome"], $row["TB_Processo_idTB_Processo"], $row["TB_Regional_idTB_Regional"]);
                //recuperar o processo
                $controlProcesso = new ControllerProcesso();
                $tip->setProcesso($controlProcesso->getProcesso($tip->getProcesso()));
                //recuperar a regional
                $controlRegional = new ControllerRegional();
                $tip->setRegional($controlRegional->getRegional($tip->getRegional()));
                $tiposProcesso[] = $tip->toArray();
            }
        } else {
            $tiposProcesso = null;
        }
        $bd->close();
        return $tiposProcesso;
    }

    public function deleteTipoProcesso($idTipoProcesso) {
        $bd = new BD();
        $sql = "DELETE FROM TB_TipoProcesso WHERE idTB_TipoProcesso = :idTipoProcesso";
        $bd->query($sql);
        $bd->bind(':idTipoProcesso', $idTipoProcesso);
        $bd->execute();
        $bd->close();
    }

    public function postTipoProcesso($dados) {
        $bd = new BD();
        $sql = "INSERT INTO TB_TipoProcesso (Nome, TB_Processo_idTB_Processo, TB_Regional_idTB_Regional) VALUES (:nome, :processo, :regional)";
        $bd->query($sql);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->bind(':processo', $dados["TB_Processo_idTB_Processo"]);
        $bd->bind(':regional', $dados["TB_Regional_idTB_Regional"]);
        $bd->execute();
         $json=array(
            'id'=>(int)$bd->lastInput()
        );
         $bd->close();
        return $json;
    }

    public function putTipoProcesso($idTipoProcesso, $dados) {
        $bd = new BD();
        $sql = "UPDATE TB_TipoProcesso SET Nome=:nome, TB_Processo_idTB_Processo=:processo, TB_Regional_idTB_Regional=:regional WHERE idTB_TipoProcesso = :idTipoProcesso";
        $bd->query($sql);
        $bd->bind(':idTipoProcesso', $idTipoProcesso);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->bind(':processo', $dados["TB_Processo_idTB_Processo"]);
        $bd->bind(':regional', $dados["TB_Regional_idTB_Regional"]);
        $bd->execute();
        $bd->close();
    }

}
