<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerLocal
 *
 * @author George Tassiano
 */
use App\Modulos\Local;
use App\Modulos\BD;
class ControllerLocal {
    public function getLocal($idLocal){
        
        $bd = new BD();
        $sql = "SELECT * FROM TB_Local WHERE idTB_Local = :idLocal";
        $bd->query($sql);
        $bd->bind(':idLocal', $idLocal);
        $bd->execute();
        
        $row=$bd->single();
        if(!empty($row)){
            $loc = new Local($row["idTB_Local"], $row["Endereco"], $row["CEP"], $row["PosicaoGeografica"], $row["Estado"], $row["Cidade"]);
            $local=$loc->toArray();
        }else{
            $local=null;
        }
        $bd->close();
        return $local;
    }
    
    public function getLocais() {
        $bd = new BD();
        $sql = "SELECT * FROM TB_Local";
        $bd->query($sql);
        if ($bd->execute()) {
            $locais=array();
            while ($row = $bd->single()) {
                $loc = new Local($row["idTB_Local"], $row["Endereco"], $row["CEP"], $row["PosicaoGeografica"], $row["Estado"], $row["Cidade"]);
                $locais[] = $loc->toArray();
            }
        }else{
            $locais=null;
        }
        $bd->close();
        return $locais;
    }
    public function deleteLocal($idLocal){
        $bd = new BD();
        $sql = "DELETE FROM TB_Local WHERE idTB_Local = :idLocal";
        $bd->query($sql);
        $bd->bind(':idLocal', $idLocal);
        $bd->execute();
        $bd->close();
    }
    
    public function postLocal($dados){
        $bd = new BD();
        $sql = "INSERT INTO TB_Local (Endereco, CEP, PosicaoGeografica, Estado, Cidade) VALUES (:endereco, :cep, :posicaoGeografica, :estado, :cidade)";
        $bd->query($sql);
        $bd->bind(':endereco', $dados["Endereco"]);
        $bd->bind(':cep', $dados["CEP"]);
        $bd->bind(':posicaoGeografica', $dados["PosicaoGeografica"]);
        $bd->bind(':estado', $dados["Estado"]);
        $bd->bind(':cidade', $dados["Cidade"]);
        $bd->execute();
         $json=array(
            'id'=>(int)$bd->lastInput()
        );
         $bd->close();
        return $json;
    }
    
    public function putLocal($idLocal, $dados){
        $bd = new BD();
        $sql = "UPDATE TB_Local SET Endereco=:endereco, CEP=:cep, PosicaoGeografica=:posicaoGeografica, Estado=:estado, Cidade=:cidade WHERE idTB_Local = :idLocal";
        $bd->query($sql);
        $bd->bind(':idLocal', $idLocal);
        $bd->bind(':endereco', $dados["Endereco"]);
        $bd->bind(':cep', $dados["CEP"]);
        $bd->bind(':posicaoGeografica', $dados["PosicaoGeografica"]);
        $bd->bind(':estado', $dados["Estado"]);
        $bd->bind(':cidade', $dados["Cidade"]);
        $bd->execute();
        $bd->close();
    }
    
}
