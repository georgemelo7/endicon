<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerDanoMaterial
 *
 * @author ADM
 */
use App\Modulos\BD;
use App\Modulos\DanoMaterial;

class ControllerDanoMaterial {
    
    public function getDanoMaterial($idDanoMaterial) {

        $bd = new BD();
        $sql = "SELECT * FROM TB_DanoMaterial WHERE idTB_DanoMaterial = :idDanoMaterial";
        $bd->query($sql);
        $bd->bind(':idDanoMaterial', $idDanoMaterial);
        $bd->execute();
        $row = $bd->single();
        if (!empty($row)) {
            $dan = new DanoMaterial($row['idTB_DanoMaterial'], $row['Nome'], $row['Custo'], $row['TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao']);
            $danoMaterial = $dan->toArray();
        } else {
            $danoMaterial = null;
        }
        $bd->close();
        return $danoMaterial;
    }

    public function getDanosMateriais() {
        $bd = new BD();
        $sql = "SELECT * FROM TB_DanoMaterial";
        $bd->query($sql);
        if ($bd->execute()) {
            $danosMateriais=array();
            while ($row = $bd->single()) {
                $dan = new DanoMaterial($row['idTB_DanoMaterial'], $row['Nome'], $row['Custo'], $row['TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao']);
                $danosMateriais[] = $dan->toArray();
            }
        }else{
            $danosMateriais=null;
        }
        $bd->close();
        return $danosMateriais;
    }
    public function deleteDanoMaterial($idDanoMaterial){
        $bd = new BD();
        $sql = "DELETE FROM TB_DanoMaterial WHERE idTB_DanoMaterial = :idDanoMaterial";
        $bd->query($sql);
        $bd->bind(':idDanoMaterial', $idDanoMaterial);
        $bd->execute();
        $bd->close();
    }
    
    public function postDanoMaterial($dados){
        $bd = new BD();
        $sql = "INSERT INTO TB_DanoMaterial (Nome, Custo, TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao) VALUES (:nome, :custo, :relatorio)";
        $bd->query($sql);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->bind(':custo', $dados["Custo"]);
        $bd->bind(':relatorio', $dados["TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao"]);
        $bd->execute();
         $json=array(
            'id'=>(int)$bd->lastInput()
        );
         $bd->close();
        return $json;
    }
    
    public function putDanoMaterial($idDanoMaterial, $dados){
        $bd = new BD();
        $sql = "UPDATE TB_DanoMaterial SET Nome=:nome, Custo=:custo, TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao=:relatorio WHERE idTB_DanoMaterial = :idDanoMaterial";
        $bd->query($sql);
        $bd->bind(':idDanoMaterial', $idDanoMaterial);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->bind(':custo', $dados["Custo"]);
        $bd->bind(':relatorio', $dados["TB_RelatorioInvestigacao_idTB_RelatorioInvestigacao"]);
        $bd->execute();
        $bd->close();
    }
    
}
