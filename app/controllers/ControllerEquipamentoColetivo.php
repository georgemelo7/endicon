<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerEquipamentoColetivo
 *
 * @author ADM
 */
use App\Modulos\BD;
use App\Modulos\Equipamento;
class ControllerEquipamentoColetivo {
    public function getEquipamento($idEquipamento){
        
        $bd = new BD();
        $sql = "SELECT * FROM TB_EquipamentoColetivo WHERE idTB_EquipamentoColetivo = :idEquipamento";
        $bd->query($sql);
        $bd->bind(':idEquipamento', $idEquipamento);
        $bd->execute();
        $row=$bd->single();
        if(!empty($row)){
            $equ = new Equipamento($row["idTB_EquipamentoColetivo"], $row["Nome"]);
            $equipamento=$equ->toArray();
        }else{
            $equipamento=null;
        }
        $bd->close();
        return $equipamento;
    }
    
    public function getEquipamentos() {
        $bd = new BD();
        $sql = "SELECT * FROM TB_EquipamentoColetivo";
        $bd->query($sql);
        if ($bd->execute()) {
            $equipamentos=array();
            while ($row = $bd->single()) {
                $equ = new Equipamento($row["idTB_EquipamentoColetivo"], $row["Nome"]);
                $equipamentos[] = $equ->toArray();
            }
        }else{
            $equipamentos=null;
        }
        $bd->close();
        return $equipamentos;
    }
    public function deleteEquipamento($idEquipamento){
        $bd = new BD();
        $sql = "DELETE FROM TB_EquipamentoColetivo WHERE idTB_EquipamentoColetivo = :idEquipamento";
        $bd->query($sql);
        $bd->bind(':idEquipamento', $idEquipamento);
        $bd->execute();
        $bd->close();
    }
    
    public function postEquipamento($dados){
        $bd = new BD();
        $sql = "INSERT INTO TB_EquipamentoColetivo (Nome) VALUES (:nome)";
        $bd->query($sql);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->execute();
         $json=array(
            'id'=>(int)$bd->lastInput()
        );
         $bd->close();
        return $json;
    }
    
    public function putProcesso($idEquipamento, $dados){
        $bd = new BD();
        $sql = "UPDATE TB_EquipamentoColetivo SET Nome=:nome WHERE idTB_EquipamentoColetivo = :idEquipamento";
        $bd->query($sql);
        $bd->bind(':idEquipamento', $idEquipamento);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->execute();
        $bd->close();
    }
}
