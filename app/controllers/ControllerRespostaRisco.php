<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerResposta
 *
 * @author ADM
 */
use App\Modulos\BD;
use App\Modulos\Resposta;
class ControllerRespostaRisco {

    public function getResposta($idReferencia, $idPergunta) {

        $bd = new BD();
        $sql = "SELECT * FROM TB_AnaliseRisco_has_TB_PerguntaRisco WHERE TB_AnaliseRisco_idTB_AnaliseRisco = :idReferencia AND TB_PerguntaRisco_idTB_PerguntaRisco= :idPergunta";
        $bd->query($sql);
        $bd->bind(':idReferencia', $idReferencia);
        $bd->bind(':idPergunta', $idPergunta);
        $bd->execute();
        $row = $bd->single();
        if (!empty($row)) {
            $res = new Resposta($row["TB_AnaliseRisco_idTB_AnaliseRisco"], $row["TB_PerguntaRisco_idTB_PerguntaRisco"], $row["Resposta"]);

            //recupera pergunta
            $controlPergunta = new ControllerPerguntaRisco();
            $res->setIdPergunta($controlPergunta->getPergunta($res->getIdPergunta()));

            $resposta = $res->toArray();
        } else {
            $resposta = null;
        }
        $bd->close();
        return $resposta;
    }

    public function getRespostas() {
        $bd = new BD();
        $sql = "SELECT * FROM TB_AnaliseRisco_has_TB_PerguntaRisco";
        $bd->query($sql);
        if ($bd->execute()) {
            $respostas = array();
            while ($row = $bd->single()) {
                $res = new Resposta($row["TB_AnaliseRisco_idTB_AnaliseRisco"], $row["TB_PerguntaRisco_idTB_PerguntaRisco"], $row["Resposta"]);
                //recupera pergunta
                $controlPergunta = new ControllerPerguntaRisco();
                $res->setIdPergunta($controlPergunta->getPergunta($res->getIdPergunta()));

                $respostas[] = $res->toArray();
            }
        } else {
            $respostas = null;
        }
        $bd->close();
        return $respostas;
    }

    public function deleteResposta($idReferencia, $idPergunta) {
        $bd = new BD();
        $sql = "SELECT * FROM TB_AnaliseRisco_has_TB_PerguntaRisco WHERE TB_AnaliseRisco_idTB_AnaliseRisco = :idReferencia AND TB_PerguntaRisco_idTB_PerguntaRisco= :idPergunta";
        $bd->query($sql);
        $bd->bind(':idReferencia', $idReferencia);
        $bd->bind(':idPergunta', $idPergunta);
        $bd->execute();
        $bd->close();
    }

    public function postResposta($dados) {
        $bd = new BD();
        $sql = "INSERT INTO TB_AnaliseRisco_has_TB_PerguntaRisco (TB_AnaliseRisco_idTB_AnaliseRisco, TB_PerguntaRisco_idTB_PerguntaRisco, Resposta) VALUES (:idReferencia, :idPergunta, :resposta)";
        $bd->query($sql);
        $bd->bind(':idReferencia', $dados["TB_AnaliseRisco_idTB_AnaliseRisco"]);
        $bd->bind(':idPergunta', $dados["TB_PerguntaRisco_idTB_PerguntaRisco"]);
        $bd->bind(':resposta', $dados["Resposta"]);
        $bd->execute();
         $json=array(
            'id'=>(int)$bd->lastInput()
        );
         $bd->close();
        return $json;
    }

    public function putResposta($idReferencia, $idPergunta, $dados) {
        $bd = new BD();
        $sql = "UPDATE TB_AnaliseRisco_has_TB_PerguntaRisco SET Resposta=:resposta WHERE TB_AnaliseRisco_idTB_AnaliseRisco = :idReferencia AND TB_PerguntaRisco_idTB_PerguntaRisco= :idPergunta";
        $bd->query($sql);
        $bd->bind(':idReferencia', $idReferencia);
        $bd->bind(':idPergunta', $idPergunta);
        $bd->bind(':resposta', $dados["Resposta"]);
        $bd->execute();
        $bd->close();
    }

}
