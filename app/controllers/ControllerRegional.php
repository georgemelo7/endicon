<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerRegional
 *
 * @author George Tassiano
 */
use App\Modulos\BD;
use App\Modulos\Regional;
class ControllerRegional {
    public function getRegional($idRegional){
        
        $bd = new BD();
        $sql = "SELECT * FROM TB_Regional WHERE idTB_Regional = :idRegional";
        $bd->query($sql);
        $bd->bind(':idRegional', $idRegional);
        $bd->execute();
        $row=$bd->single();
        if(!empty($row)){
            $reg = new Regional($row["idTB_Regional"], $row["Nome"], $row["Estado"]);
            $regional=$reg->toArray();
        }else{
            $regional=null;
        }
        $bd->close();
        return $regional;
    }
    
    public function getRegionais() {
        $bd = new BD();
        $sql = "SELECT * FROM TB_Regional";
        $bd->query($sql);
        if ($bd->execute()) {
            $regionais=array();
            while ($row = $bd->single()) {
                $reg = new Regional($row["idTB_Regional"], $row["Nome"], $row["Estado"]);
                $regionais[] = $reg->toArray();
            }
        }else{
            $regionais=null;
        }
        $bd->close();
        return $regionais;
    }
    public function deleteRegional($idRegional){
        $bd = new BD();
        $sql = "DELETE FROM TB_Regional WHERE idTB_Regional = :idRegional";
        $bd->query($sql);
        $bd->bind(':idRegional', $idRegional);
        $bd->execute();
        $bd->close();
    }
    
    public function postRegional($dados){
        $bd = new BD();
        $sql = "INSERT INTO TB_Regional (Nome, Estado) VALUES (:nome, :estado)";
        $bd->query($sql);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->bind(':estado', $dados["Estado"]);
        $bd->execute();
        $json=array(
            'id'=>(int)$bd->lastInput()
        );
        $bd->close();
        return $json;
    }
    
    public function putRegional($idRegional, $dados){
        $bd = new BD();
        $sql = "UPDATE TB_Regional SET Nome=:nome, Estado=:estado WHERE idTB_Regional = :idRegional";
        $bd->query($sql);
        $bd->bind(':idRegional', $idRegional);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->bind(':estado', $dados["Estado"]);
        $bd->execute();
        $bd->close();
    }
}
