<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerRiscoIdentificado
 *
 * @author ADM
 */
use App\Modulos\BD;
use App\Modulos\RiscoIdentificado;
class ControllerRiscoIdentificado {
    public function getRiscoIdentificado($idRiscoIdentificado){
        
        $bd = new BD();
        $sql = "SELECT * FROM TB_RiscoIdentificado WHERE idTB_RiscoIdentificado = :idRiscoIdentificado";
        $bd->query($sql);
        $bd->bind(':idRiscoIdentificado', $idRiscoIdentificado);
        $bd->execute();
        $row=$bd->single();
        if(!empty($row)){
            $ris = new RiscoIdentificado($row["idTB_RiscoIdentificado"], $row["Nome"]);
            $riscoIdentificado=$ris->toArray();
        }else{
            $riscoIdentificado=null;
        }
        $bd->close();
        return $riscoIdentificado;
    }
    
    public function getRiscosIdentificados() {
        $bd = new BD();
        $sql = "SELECT * FROM TB_RiscoIdentificado";
        $bd->query($sql);
        if ($bd->execute()) {
            $riscosIdentificados=array();
            while ($row = $bd->single()) {
                $ris = new RiscoIdentificado($row["idTB_RiscoIdentificado"], $row["Nome"]);
                $riscosIdentificados[] = $ris->toArray();
            }
        }else{
            $riscosIdentificados=null;
        }
        $bd->close();
        return $riscosIdentificados;
    }
    public function deleteRiscoIdentificado($idRiscoIdentificado){
        $bd = new BD();
        $sql = "DELETE FROM TB_RiscoIdentificado WHERE idTB_RiscoIdentificado = :idRiscoIdentificado";
        $bd->query($sql);
        $bd->bind(':idRiscoIdentificado', $idRiscoIdentificado);
        $bd->execute();
        $bd->close();
    }
    
    public function postRiscoIdentificado($dados){
        $bd = new BD();
        $sql = "INSERT INTO TB_RiscoIdentificado (Nome) VALUES (:nome)";
        $bd->query($sql);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->execute();
         $json=array(
            'id'=>(int)$bd->lastInput()
        );
         $bd->close();
        return $json;
    }
    
    public function putRiscoIdentificado($idRiscoIdentificado, $dados){
        $bd = new BD();
        $sql = "UPDATE TB_RiscoIdentificado SET Nome=:nome WHERE idTB_RiscoIdentificado = :idRiscoIdentificado";
        $bd->query($sql);
        $bd->bind(':idRiscoIdentificado', $idRiscoIdentificado);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->execute();
        $bd->close();
    }
}
