<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerAnaliseRisco
 *
 * @author George Tassiano
 */
use App\Modulos\AnaliseRisco;
use App\Modulos\BD;

class ControllerAnaliseRisco {

    public function getAnaliseRisco($idAnaliseRisco) {

        $bd = new BD();
        $sql = "SELECT * FROM TB_AnaliseRisco WHERE idTB_AnaliseRisco = :idAnaliseRisco";
        $bd->query($sql);
        $bd->bind(':idAnaliseRisco', $idAnaliseRisco);
        $bd->execute();
        $row = $bd->single();
        $bd->close();
        if (!empty($row)) {
            $ana = new AnaliseRisco($row["idTB_AnaliseRisco"], $row["DataCriacao"], $row["HoraCriacao"], $row["DescricaoAtividade"], $row["Observacao"], $row["TB_Local_idTB_Local"], $row["TB_Funcionario_idTB_Funcionario"], $row["TB_TipoProcesso_idTB_TipoProcesso"]);

            //recupera perguntasRespondidas
            $controlResposta = new ControllerRespostaRisco();
            $bd = new BD();
            $sql = "SELECT * FROM TB_AnaliseRisco_has_TB_PerguntaRisco WHERE TB_AnaliseRisco_idTB_AnaliseRisco=:idAnaliseRisco";
            $bd->query($sql);
            $bd->bind(':idAnaliseRisco', $ana->getId());
            if ($bd->execute()) {
                $perguntasRespondidas = array();
                while ($row = $bd->single()) {
                    $perguntasRespondidas[] = $controlResposta->getResposta($ana->getId(), $row["TB_PerguntaRisco_idTB_PerguntaRisco"]);
                }
            } else {
                $perguntasRespondidas = null;
            }
            $ana->setPerguntasRespondidas($perguntasRespondidas);
            $bd->close();
            //equipamentosIndividuais
            $controlEquipamento = new ControllerEquipamentoIndividual();
            $bd = new BD();
            $sql = "SELECT * FROM TB_AnaliseRisco_has_TB_EquipamentoIndividual WHERE TB_AnaliseRisco_idTB_AnaliseRisco=:idAnaliseRisco";
            $bd->query($sql);
            $bd->bind(':idAnaliseRisco', $ana->getId());
            if ($bd->execute()) {
                $equipamentosIndividuais = array();
                while ($row = $bd->single()) {
                    $equipamentosIndividuais[] = $controlEquipamento->getEquipamento($row["TB_EquipamentoIndividual_idTB_EquipamentoIndividual"]);
                }
            } else {
                $equipamentosIndividuais = null;
            }
            $ana->setEquipamentosIndividuais($equipamentosIndividuais);
            $bd->close();
            //equipamentosColetivos
            $controlEquipamento = new ControllerEquipamentoColetivo();
            $bd = new BD();
            $sql = "SELECT * FROM TB_AnaliseRisco_has_TB_EquipamentoColetivo WHERE TB_AnaliseRisco_idTB_AnaliseRisco=:idAnaliseRisco";
            $bd->query($sql);
            $bd->bind(':idAnaliseRisco', $ana->getId());
            if ($bd->execute()) {
                $equipamentosColetivos = array();
                while ($row = $bd->single()) {
                    $equipamentosColetivos[] = $controlEquipamento->getEquipamento($row["TB_EquipamentoColetivo_idTB_EquipamentoColetivo"]);
                }
            } else {
                $equipamentosColetivos = null;
            }
            $ana->setEquipamentosColetivos($equipamentosColetivos);
            $bd->close();
            //riscosIdentificados
            $controlRiscoIdentificado = new ControllerRiscoIdentificado();
            $bd = new BD();
            $sql = "SELECT * FROM TB_AnaliseRisco_has_TB_IdentificacaoRisco WHERE TB_AnaliseRisco_idTB_AnaliseRisco=:idAnaliseRisco";
            $bd->query($sql);
            $bd->bind(':idAnaliseRisco', $ana->getId());
            if ($bd->execute()) {
                $riscosIdentificados = array();
                while ($row = $bd->single()) {
                    $riscosIdentificados[] = $controlRiscoIdentificado->getRiscoIdentificado($row["TB_IdentificacaoRisco_idTB_IdentificacaoRisco"]);
                }
            } else {
                $riscosIdentificados = null;
            }
            $ana->setRiscosIdentificados($riscosIdentificados);
            $bd->close();
            //membros
            $controlFuncionario = new ControllerFuncionario();
            $bd = new BD();
            $sql = "SELECT * FROM TB_Funcionario_has_TB_AnaliseRisco WHERE TB_AnaliseRisco_idTB_AnaliseRisco=:idAnaliseRisco";
            $bd->query($sql);
            $bd->bind(':idAnaliseRisco', $ana->getId());
            if ($bd->execute()) {
                $funcionarios = array();
                while ($row = $bd->single()) {
                    $funcionarios[] = $controlFuncionario->getFuncionario($row["TB_Funcionario_idTB_Funcionario"]);
                }
            } else {
                $funcionarios = null;
            }
            $ana->setMembros($funcionarios);
            $bd->close();

            $analiseRisco = $ana->toArray();
        } else {
            $analiseRisco = null;
        }
        
        return $analiseRisco;
    }

    public function getAnalisesRisco() {
        $bd1 = new BD();
        $sql = "SELECT * FROM TB_AnaliseRisco";
        $bd1->query($sql);
        if ($bd1->execute()) {
            $analisesRisco = array();
            while ($row = $bd1->single()) {
                $ana = new AnaliseRisco($row["idTB_AnaliseRisco"], $row["DataCriacao"], $row["HoraCriacao"], $row["DescricaoAtividade"], $row["Observacao"], $row["TB_Local_idTB_Local"], $row["TB_Funcionario_idTB_Funcionario"], $row["TB_TipoProcesso_idTB_TipoProcesso"]);
                
                //recupera o local
                $controlLocal = new ControllerLocal;
                $ana->setLocal($controlLocal->getLocal($ana->getLocal()));
                //recupera funcionario
                $controlFuncionario = new ControllerFuncionario;
                $ana->setFuncionario($controlFuncionario->getFuncionario($ana->getFuncionario()));
                //recupera tipoprocesso
                $controlTipoProcesso = new ControllerTipoProcesso();
                $ana->setTipoProcesso($controlTipoProcesso->getTipoProcesso($ana->getTipoProcesso()));
                //recupera perguntasRespondidas
                $controlResposta = new ControllerRespostaRisco();
                $bd = new BD();
                $sql = "SELECT * FROM TB_AnaliseRisco_has_TB_PerguntaRisco WHERE TB_AnaliseRisco_idTB_AnaliseRisco=:idAnaliseRisco";
                $bd->query($sql);
                $bd->bind(':idAnaliseRisco', $ana->getId());
                if ($bd->execute()) {
                    $perguntasRespondidas = array();
                    while ($row = $bd->single()) {
                        $perguntasRespondidas[] = $controlResposta->getResposta($ana->getId(), $row["TB_PerguntaRisco_idTB_PerguntaRisco"]);
                    }
                } else {
                    $perguntasRespondidas = null;
                }
                $ana->setPerguntasRespondidas($perguntasRespondidas);
                $bd->close();
                //equipamentosIndividuais
                $controlEquipamento = new ControllerEquipamentoIndividual();
                $bd = new BD();
                $sql = "SELECT * FROM TB_AnaliseRisco_has_TB_EquipamentoIndividual WHERE TB_AnaliseRisco_idTB_AnaliseRisco=:idAnaliseRisco";
                $bd->query($sql);
                $bd->bind(':idAnaliseRisco', $ana->getId());
                if ($bd->execute()) {
                    $equipamentosIndividuais = array();
                    while ($row = $bd->single()) {
                        $equipamentosIndividuais[] = $controlEquipamento->getEquipamento($row["TB_EquipamentoIndividual_idTB_EquipamentoIndividual"]);
                    }
                } else {
                    $equipamentosIndividuais = null;
                }
                $ana->setEquipamentosIndividuais($equipamentosIndividuais);
                $bd->close();
                //equipamentosColetivos
                $controlEquipamento = new ControllerEquipamentoColetivo();
                $bd = new BD();
                $sql = "SELECT * FROM TB_AnaliseRisco_has_TB_EquipamentoColetivo WHERE TB_AnaliseRisco_idTB_AnaliseRisco=:idAnaliseRisco";
                $bd->query($sql);
                $bd->bind(':idAnaliseRisco', $ana->getId());
                if ($bd->execute()) {
                    $equipamentosColetivos = array();
                    while ($row = $bd->single()) {
                        $equipamentosColetivos[] = $controlEquipamento->getEquipamento($row["TB_EquipamentoColetivo_idTB_EquipamentoColetivo"]);
                    }
                } else {
                    $equipamentosColetivos = null;
                }
                $ana->setEquipamentosColetivos($equipamentosColetivos);
                $bd->close();
                //riscosIdentificados
                $controlRiscoIdentificado = new ControllerRiscoIdentificado();
                $bd = new BD();
                $sql = "SELECT * FROM TB_AnaliseRisco_has_TB_RiscoIdentificado WHERE TB_AnaliseRisco_idTB_AnaliseRisco=:idAnaliseRisco";
                $bd->query($sql);
                $bd->bind(':idAnaliseRisco', $ana->getId());
                if ($bd->execute()) {
                    $riscosIdentificados = array();
                    while ($row = $bd->single()) {
                        $riscosIdentificados[] = $controlRiscoIdentificado->getRiscoIdentificado($row["TB_RiscoIdentificado_idTB_RiscoIdentificado"]);
                    }
                } else {
                    $riscosIdentificados = null;
                }
                $ana->setRiscosIdentificados($riscosIdentificados);
                $bd->close();
                //membros
                $controlFuncionario = new ControllerFuncionario();
                $bd = new BD();
                $sql = "SELECT * FROM TB_Funcionario_has_TB_AnaliseRisco WHERE TB_AnaliseRisco_idTB_AnaliseRisco=:idAnaliseRisco";
                $bd->query($sql);
                $bd->bind(':idAnaliseRisco', $ana->getId());
                if ($bd->execute()) {
                    $funcionarios = array();
                    while ($row = $bd->single()) {
                        $funcionarios[] = $controlFuncionario->getFuncionario($row["TB_Funcionario_idTB_Funcionario"]);
                    }
                } else {
                    $funcionarios = null;
                }
                $ana->setMembros($funcionarios);
                $bd->close();

                $analisesRisco[] = $ana->toArray();
            }
        } else {
            $analisesRisco = null;
        }
        $bd1->close();
        return $analisesRisco;
    }

    public function deleteAnaliseRisco($idAnaliseRisco) {
        $bd = new BD();
        $sql = "DELETE FROM TB_AnaliseRisco WHERE idTB_AnaliseRisco = :idAnaliseRisco";
        $bd->query($sql);
        $bd->bind(':idAnaliseRisco', $idAnaliseRisco);
        $bd->execute();
        $bd->close();
    }

    public function postAnaliseRisco($dados) {
        $bd = new BD();
        $sql = "INSERT INTO TB_AnaliseRisco (DataCriacao, HoraCriacao, DescricaoAtividade, Observacao, TB_Local_idTB_Local, TB_Funcionario_idTB_Funcionario, TB_TipoProcesso_idTB_TipoProcesso) VALUES (:dataCriacao, :horaCriacao, :descricaoAtividade, :observacao, :local, :funcionario, :tipoProcesso)";
        $bd->query($sql);
        $bd->bind(':dataCriacao', $dados["DataCriacao"]);
        $bd->bind(':horaCriacao', $dados["HoraCriacao"]);
        $bd->bind(':descricaoAtividade', $dados["DescricaoAtividade"]);
        $bd->bind(':observacao', $dados["Observacao"]);
        $bd->bind(':local', $dados["TB_Local_idTB_Local"]);
        $bd->bind(':funcionario', $dados["TB_Funcionario_idTB_Funcionario"]);
        $bd->bind(':tipoProcesso', $dados["TB_TipoProcesso_idTB_TipoProcesso"]);
        $bd->execute();
         $json=array(
            'id'=>(int)$bd->lastInput()
        );
         $bd->close();
        return $json;
    }

    public function putAnaliseRisco($idAnaliseRisco, $dados) {
        $bd = new BD();
        $sql = "UPDATE TB_AnaliseRisco SET DataCriacao=:dataCriacao, HoraCriacao=:horaCriacao, DescricaoAtividade=:descricaoAtividade, Observacao=:observacao, TB_Local_idTB_Local=:local, TB_Funcionario_idTB_Funcionario=:funcionario, TB_TipoProcesso_idTB_TipoProcesso=:tipoProcesso WHERE idTB_AnaliseRisco = :idAnaliseRisco";
        $bd->query($sql);
        $bd->bind(':idAnaliseRisco', $idAnaliseRisco);
        $bd->bind(':dataCriacao', $dados["DataCriacao"]);
        $bd->bind(':horaCriacao', $dados["HoraCriacao"]);
        $bd->bind(':descricaoAtividade', $dados["DescricaoAtividade"]);
        $bd->bind(':observacao', $dados["Observacao"]);
        $bd->bind(':local', $dados["TB_Local_idTB_Local"]);
        $bd->bind(':funcionario', $dados["TB_Funcionario_idTB_Funcionario"]);
        $bd->bind(':tipoProcesso', $dados["TB_TipoProcesso_idTB_TipoProcesso"]);
        $bd->execute();
        $bd->close();
    }

}
