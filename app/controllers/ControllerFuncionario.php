<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

use App\Modulos\BD;
use App\Modulos\Funcionario;

class ControllerFuncionario {

    public function getFuncionario($idFuncionario) {

        $bd = new BD();
        $sql = "SELECT * FROM TB_Funcionario WHERE idTB_Funcionario = :idFuncionario";
        $bd->query($sql);
        $bd->bind(':idFuncionario', $idFuncionario);
        $bd->execute();
        $row = $bd->single();
        if (!empty($row)) {

            $fun = new Funcionario($row["idTB_Funcionario"], $row["Nome"], $row["Matricula"], $row["DataNascimento"], $row["Sexo"], $row["EstadoCivil"], $row["InicioFuncao"], $row["GrauInstrucao"], $row["EstadoInstrucao"], $row["InicioRefeicao"], $row["FinalRefeicao"], $row["TurnoTrabalho"], $row["Nacionalidade"], $row["TB_Funcao_idTB_Funcao"], $row["TB_Local_idTB_Local"], $row["TB_LocalidadePolo_idTB_LocalidadePolo"]);
            //recupera funcao
            $controlFuncao = new ControllerFuncao();
            $fun->setFuncao($controlFuncao->getFuncao($fun->getFuncao()));
            //recupera local
            $controlLocal = new ControllerLocal();
            $fun->setLocal($controlLocal->getLocal($fun->getLocal()));
            //recupera localidadePolo
            $controlLocalidadePolo = new ControllerLocalidadePolo();
            $fun->setLocalidadePolo($controlLocalidadePolo->getLocalidadePolo($fun->getLocalidadePolo()));
            //recupera treinamentosRealizados
            $controlTreinamentoRealizado = new ControllerTreinamentoRealizado();
            $bd = new BD();
            $sql = "SELECT * FROM TB_Funcionario_has_TB_Treinamento WHERE TB_Funcionario_idTB_Funcionario=:idFuncionario";
            $bd->query($sql);
            $bd->bind(':idFuncionario', $fun->getId());
            if ($bd->execute()) {
                $treinamentosRealizados = array();
                while ($row = $bd->single()) {
                    $treinamentosRealizados[] = $controlTreinamentoRealizado->getTreinamentoRealizado($fun->getId(), $row["TB_Treinamento_idTB_Treinamento"]);
                }
            } else {
                $treinamentosRealizados = null;
            }
            $fun->setTreinamentosRealizados($treinamentosRealizados);
            $bd->close();
            $funcionario = $fun->toArray();
        } else {
            $funcionario = null;
        }
        $bd->close();

        return $funcionario;
    }
    
    public function getFuncionarioLite($idFuncionario) {

        $bd = new BD();
        $sql = "SELECT * FROM TB_Funcionario WHERE idTB_Funcionario = :idFuncionario";
        $bd->query($sql);
        $bd->bind(':idFuncionario', $idFuncionario);
        $bd->execute();
        $row = $bd->single();
        if (!empty($row)) {

            $fun = new Funcionario($row["idTB_Funcionario"], $row["Nome"], $row["Matricula"], $row["DataNascimento"], $row["Sexo"], $row["EstadoCivil"], $row["InicioFuncao"], $row["GrauInstrucao"], $row["EstadoInstrucao"], $row["InicioRefeicao"], $row["FinalRefeicao"], $row["TurnoTrabalho"], $row["Nacionalidade"], $row["TB_Funcao_idTB_Funcao"], $row["TB_Local_idTB_Local"], $row["TB_LocalidadePolo_idTB_LocalidadePolo"]);
            
            $funcionario = $fun->toArray();
        } else {
            $funcionario = null;
        }
        $bd->close();

        return $funcionario;
    }

    public function getFuncionarios() {
        $bd1 = new BD();
        $sql = "SELECT * FROM TB_Funcionario";
        $bd1->query($sql);
        if ($bd1->execute()) {
            $funcionarios = array();
            while ($row = $bd1->single()) {
                $fun = new Funcionario($row["idTB_Funcionario"], $row["Nome"], $row["Matricula"], $row["DataNascimento"], $row["Sexo"], $row["EstadoCivil"], $row["InicioFuncao"], $row["GrauInstrucao"], $row["EstadoInstrucao"], $row["InicioRefeicao"], $row["FinalRefeicao"], $row["TurnoTrabalho"], $row["Nacionalidade"], $row["TB_Funcao_idTB_Funcao"], $row["TB_Local_idTB_Local"], $row["TB_LocalidadePolo_idTB_LocalidadePolo"]);
                //recupera funcao
                $controlFuncao = new ControllerFuncao();
                $fun->setFuncao($controlFuncao->getFuncao($fun->getFuncao()));
                //recupera local
                $controlLocal = new ControllerLocal();
                $fun->setLocal($controlLocal->getLocal($fun->getLocal()));
                //recupera localidadePolo
                $controlLocalidadePolo = new ControllerLocalidadePolo();
                $fun->setLocalidadePolo($controlLocalidadePolo->getLocalidadePolo($fun->getLocalidadePolo()));
                //recupera treinamentosRealizados
                $controlTreinamentoRealizado = new ControllerTreinamentoRealizado();
                $bd = new BD();
                $sql = "SELECT * FROM TB_Funcionario_has_TB_Treinamento WHERE TB_Funcionario_idTB_Funcionario=:idFuncionario";
                $bd->query($sql);
                $bd->bind(':idFuncionario', $fun->getId());
                if ($bd->execute()) {
                    $treinamentosRealizados = array();
                    while ($row = $bd->single()) {
                        $treinamentosRealizados[] = $controlTreinamentoRealizado->getTreinamentoRealizado($fun->getId(), $row["TB_Treinamento_idTB_Treinamento"]);
                    }
                } else {
                    $treinamentosRealizados = null;
                }
                $fun->setTreinamentosRealizados($treinamentosRealizados);
                $bd->close();

                $funcionarios[] = $fun->toArray();
            }
        } else {
            $funcionarios = null;
        }
        $bd1->close();
        return $funcionarios;
    }

    public function deleteFuncionario($idFuncionario) {
        $bd = new BD();
        $sql = "DELETE FROM TB_Funcionario WHERE idTB_Funcionario = :idFuncionario";
        $bd->query($sql);
        $bd->bind(':idFuncionario', $idFuncionario);
        $bd->execute();
        $bd->close();
    }

    public function postFuncionario($dados) {
        $bd = new BD();
        $sql = "INSERT INTO TB_Funcionario (Nome, Matricula, DataNascimento, Sexo, EstadoCivil, InicioFuncao, GrauInstrucao, EstadoInstrucao, InicioRefeicao, FinalRefeicao, TurnoTrabalho, Nacionalidade, TB_Local_idTB_Local, TB_Funcao_idTB_Funcao, TB_LocalidadePolo_idTB_LocalidadePolo) VALUES (:nome, :matricula, :dataNascimento, :sexo, :estadoCivil, :inicioFuncao, :grauInstrucao, :estadoInstrucao, :inicioRefeicao, :finalRefeicao, :turnoTrabalho, :nacionalidade, :local, :funcao, :localidadePolo)";
        $bd->query($sql);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->bind(':matricula', $dados["Matricula"]);
        $bd->bind(':dataNascimento', $dados["DataNascimento"]);
        $bd->bind(':sexo', $dados["Sexo"]);
        $bd->bind(':estadoCivil', $dados["EstadoCivil"]);
        $bd->bind(':inicioFuncao', $dados["InicioFuncao"]);
        $bd->bind(':grauInstrucao', $dados["GrauInstrucao"]);
        $bd->bind(':estadoInstrucao', $dados["EstadoInstrucao"]);
        $bd->bind(':inicioRefeicao', $dados["InicioRefeicao"]);
        $bd->bind(':finalRefeicao', $dados["FinalRefeicao"]);
        $bd->bind(':turnoTrabalho', $dados["TurnoTrabalho"]);
        $bd->bind(':nacionalidade', $dados["Nacionalidade"]);
        $bd->bind(':local', $dados["TB_Local_idTB_Local"]);
        $bd->bind(':funcao', $dados["TB_Funcao_idTB_Funcao"]);
        $bd->bind(':localidadePolo', $dados["TB_LocalidadePolo_idTB_LocalidadePolo"]);
        $bd->execute();
         $json=array(
            'id'=>(int)$bd->lastInput()
        );
         $bd->close();
        return $json;
    }

    public function putFuncionario($idFuncionario, $dados) {
        $bd = new BD();
        $sql = "UPDATE TB_Funcionario SET Nome=:nome, Matricula=:matricula, DataNascimento=:dataNascimento, Sexo=:sexo, EstadoCivil=:estadoCivil, InicioFuncao=:inicioFuncao, GrauInstrucao=:grauInstrucao, EstadoInstrucao=:estadoInstrucao, InicioRefeicao=:inicioRefeicao, FinalRefeicao=:finalRefeicao, TurnoTrabalho=:turnoTrabalho, Nacionalidade=:nacionalidade, TB_Local_idTB_Local=:local, TB_Funcao_idTB_Funcao=:funcao, TB_LocalidadePolo_idTB_LocalidadePolo=:localidadePolo WHERE idTB_Funcionario = :idFuncionario";
        $bd->query($sql);
        $bd->bind(':idFuncionario', $idFuncionario);
        $bd->bind(':nome', $dados["Nome"]);
        $bd->bind(':matricula', $dados["Matricula"]);
        $bd->bind(':dataNascimento', $dados["DataNascimento"]);
        $bd->bind(':sexo', $dados["Sexo"]);
        $bd->bind(':estadoCivil', $dados["EstadoCivil"]);
        $bd->bind(':inicioFuncao', $dados["InicioFuncao"]);
        $bd->bind(':grauInstrucao', $dados["GrauInstrucao"]);
        $bd->bind(':estadoInstrucao', $dados["EstadoInstrucao"]);
        $bd->bind(':inicioRefeicao', $dados["InicioRefeicao"]);
        $bd->bind(':finalRefeicao', $dados["FinalRefeicao"]);
        $bd->bind(':turnoTrabalho', $dados["TurnoTrabalho"]);
        $bd->bind(':nacionalidade', $dados["Nacionalidade"]);
        $bd->bind(':local', $dados["TB_Local_idTB_Local"]);
        $bd->bind(':funcao', $dados["TB_Funcao_idTB_Funcao"]);
        $bd->bind(':localidadePolo', $dados["TB_LocalidadePolo_idTB_LocalidadePolo"]);
        $bd->execute();
        $bd->close();
    }

}
