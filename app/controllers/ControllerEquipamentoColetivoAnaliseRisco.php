<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerEquipamentosColetivosAnaliseRisco
 *
 * @author ADM
 */
use App\Modulos\BD;
class ControllerEquipamentoColetivoAnaliseRisco {
    public function deleteEquipamentosColetivosAnaliseRisco($referencia){
        $bd = new BD();
        $sql = "DELETE FROM TB_AnaliseRisco_has_TB_EquipamentoColetivo WHERE TB_AnaliseRisco_idTB_AnaliseRisco = :idAnaliseRisco";
        $bd->query($sql);
        $bd->bind(':idAnaliseRisco', $referencia);
        $bd->execute();
        $bd->close();
    }
    
    public function postEquipamentoColetivoAnaliseRisco($referencia, $equipamentoColetivo){
        $bd = new BD();
        $sql = "INSERT INTO TB_AnaliseRisco_has_TB_EquipamentoColetivo (TB_AnaliseRisco_idTB_AnaliseRisco, TB_EquipamentoColetivo_idTB_EquipamentoColetivo) VALUES (:idAnaliseRisco, :idEquipamentoColetivo)";
        $bd->query($sql);
        $bd->bind(':idAnaliseRisco', $referencia);
        $bd->bind(':idEquipamentoColetivo', $equipamentoColetivo);
        $bd->execute();
        $json=array(
            'id'=>(int)$bd->lastInput()
        );
        $bd->close();
        return $json;
    }
}
