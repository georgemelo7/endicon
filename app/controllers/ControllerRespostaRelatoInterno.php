<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controllers;

/**
 * Description of ControllerResposta
 *
 * @author ADM
 */
use App\Modulos\BD;
use App\Modulos\Resposta;
class ControllerRespostaRelatoInterno {

    public function getResposta($idReferencia, $idPergunta) {

        $bd = new BD();
        $sql = "SELECT * FROM TB_RelatoInterno_has_TB_PerguntaRelato WHERE TB_RelatoInterno_idTB_RelatoInterno = :idReferencia AND TB_PerguntaRelato_idTB_PerguntaRelato= :idPergunta";
        $bd->query($sql);
        $bd->bind(':idReferencia', $idReferencia);
        $bd->bind(':idPergunta', $idPergunta);
        $bd->execute();
        $row = $bd->single();
        if (!empty($row)) {
            $res = new Resposta($row["TB_RelatoInterno_idTB_RelatoInterno"], $row["TB_PerguntaRelato_idTB_PerguntaRelato"], $row["Resposta"]);

            //recupera pergunta
            $controlPergunta = new ControllerPerguntaRelatoInterno();
            $res->setIdPergunta($controlPergunta->getPergunta($res->getIdPergunta()));

            $resposta = $res->toArray();
        } else {
            $resposta = null;
        }
        $bd->close();
        return $resposta;
    }

    public function getRespostas() {
        $bd = new BD();
        $sql = "SELECT * FROM TB_RelatoInterno_has_TB_PerguntaRelato";
        $bd->query($sql);
        if ($bd->execute()) {
            $respostas = array();
            while ($row = $bd->single()) {
                $res = new Resposta($row["TB_RelatoInterno_idTB_RelatoInterno"], $row["TB_PerguntaRelato_idTB_PerguntaRelato"], $row["Resposta"]);
                //recupera pergunta
                $controlPergunta = new ControllerPerguntaRelatoInterno();
                $res->setIdPergunta($controlPergunta->getPergunta($res->getIdPergunta()));

                $respostas[] = $res->toArray();
            }
        } else {
            $respostas = null;
        }
        $bd->close();
        return $respostas;
    }

    public function deleteResposta($idReferencia, $idPergunta) {
        $bd = new BD();
        $sql = "SELECT * FROM TB_RelatoInterno_has_TB_PerguntaRelato WHERE TB_RelatoInterno_idTB_RelatoInterno = :idReferencia AND TB_PerguntaRelato_idTB_PerguntaRelato= :idPergunta";
        $bd->query($sql);
        $bd->bind(':idReferencia', $idReferencia);
        $bd->bind(':idPergunta', $idPergunta);
        $bd->execute();
        $bd->close();
    }

    public function postResposta($dados) {
        $bd = new BD();
        $sql = "INSERT INTO TB_RelatoInterno_has_TB_PerguntaRelato (TB_RelatoInterno_idTB_RelatoInterno, TB_PerguntaRelato_idTB_PerguntaRelato, Resposta) VALUES (:idReferencia, :idPergunta, :resposta)";
        $bd->query($sql);
        $bd->bind(':idReferencia', $dados["TB_RelatoInterno_idTB_RelatoInterno"]);
        $bd->bind(':idPergunta', $dados["TB_PerguntaRelato_idTB_PerguntaRelato"]);
        $bd->bind(':resposta', $dados["Resposta"]);
        $bd->execute();
         $json=array(
            'id'=>(int)$bd->lastInput()
        );
         $bd->close();
        return $json;
    }

    public function putResposta($idReferencia, $idPergunta, $dados) {
        $bd = new BD();
        $sql = "UPDATE TB_RelatoInterno_has_TB_PerguntaRelato SET Resposta=:resposta WHERE TB_RelatoInterno_idTB_RelatoInterno = :idReferencia AND TB_PerguntaRelato_idTB_PerguntaRelato= :idPergunta";
        $bd->query($sql);
        $bd->bind(':idReferencia', $idReferencia);
        $bd->bind(':idPergunta', $idPergunta);
        $bd->bind(':resposta', $dados["Resposta"]);
        $bd->execute();
        $bd->close();
    }

}
